package com.example.idwalletscanner.data

import kotlin.reflect.full.memberProperties

data class VerifyData(
    val firstName: String?,
    val lastname: String?,
    val birthName: String?,
    val birthDate: String?,
    val birthPlace: String?,
    val address: String?,
    val nationality: String?,
    val idCardNr: String?,
    val issuingState: String?,
    val expirationDate: String?,
    val withPhoto: Boolean = true
) {

    fun toVerifyDataItemList(): List<VerifyDataItem> {
        return VerifyData::class.memberProperties.filter { member -> member.get(this) != null && member.name != "withPhoto" }
            .map { member ->
                VerifyDataItem(
                    titleForVerifyDataProperty(member.name),
                    member.get(this).toString()
                )
            }.sortedBy { it.getCompareValue() }
    }

    companion object {

        fun titleForVerifyDataProperty(propertyName: String): String {
            return when (propertyName) {
                "firstName" -> "Vorname"
                "lastname" -> "Familienname"
                "birthName" -> "Geburtsname"
                "birthDate" -> "Geburtsdatum"
                "birthPlace" -> "Geburtsort"
                "address" -> "Addresse"
                "nationality" -> "Nationalität"
                "idCardNr" -> "Ausweisnummer"
                "issuingState" -> "Ausstellerstaat"
                "expirationDate" -> "Ablaufdatum"
                else -> "Unbekannt"
            }
        }
    }
}
