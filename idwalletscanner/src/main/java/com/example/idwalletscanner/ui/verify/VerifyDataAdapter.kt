package com.example.idwalletscanner.ui.verify

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.idwalletscanner.R
import com.example.idwalletscanner.data.VerifyDataItem

class VerifyDataAdapter(private val data: List<VerifyDataItem>) :
    RecyclerView.Adapter<VerifyDataAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView
        val value: TextView

        init {
            title = view.findViewById(R.id.verifyDataItemTitle)
            value = view.findViewById(R.id.verifyDataItemValue)
        }

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.verify_data_item, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.title.text = data[position].title
        viewHolder.value.text = data[position].value
    }

    override fun getItemCount() = data.size


}