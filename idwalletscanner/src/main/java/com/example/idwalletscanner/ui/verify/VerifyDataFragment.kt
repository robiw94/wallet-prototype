package com.example.idwalletscanner.ui.verify

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.idwalletscanner.R
import com.example.idwalletscanner.data.VerifyData
import com.example.idwalletscanner.databinding.FragmentVerifyDataV2Binding
import com.google.gson.Gson

class VerifyDataFragment : Fragment() {
    private var _binding: FragmentVerifyDataV2Binding? = null
    private val binding get() = _binding!!

    private lateinit var verifyData: VerifyData

    private val args: VerifyDataFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        verifyData = Gson().fromJson(args.data, VerifyData::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentVerifyDataV2Binding.inflate(inflater, container, false)

        if(!verifyData.withPhoto) {
            binding.imageViewPhoto.visibility = View.INVISIBLE
        }

        binding.verifyDataRecyclerView.apply {
            layoutManager = LinearLayoutManager(this.context)
            adapter = VerifyDataAdapter(verifyData.toVerifyDataItemList())
        }


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

  //      requireActivity().onBackPressedDispatcher.addCallback {
  //          if(isEnabled){
  //              isEnabled = false
  //              findNavController().popBackStack()
  //          }
  //      }

        binding.buttonBackToWallet.setOnClickListener {
            findNavController().navigate(R.id.action_verifyDataFragment_to_verifyScanFragment)
        }

    }
}