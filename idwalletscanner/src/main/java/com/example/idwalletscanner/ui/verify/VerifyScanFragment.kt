package com.example.idwalletscanner.ui.verify

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import androidx.fragment.app.Fragment
import com.example.idwalletscanner.R
import com.example.idwalletscanner.databinding.FragmentVerifyScanBinding
import com.google.android.gms.common.moduleinstall.ModuleInstall
import com.google.android.gms.common.moduleinstall.ModuleInstallRequest
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning

class VerifyScanFragment : Fragment() {

    private var _binding: FragmentVerifyScanBinding? = null
    private val binding get() = _binding!!

    private var runnable: Runnable? = null
    private lateinit var scannedData: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentVerifyScanBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

  //      requireActivity().onBackPressedDispatcher.addCallback {
  //          if(isEnabled){
  //              isEnabled = false
  //              findNavController().popBackStack()
  //          }
  //      }

        binding.buttonStartScan.setOnClickListener {


            val moduleInstallClient = ModuleInstall.getClient(requireContext())
            val optionalModuleApi = GmsBarcodeScanning.getClient(requireContext())
            moduleInstallClient
                .areModulesAvailable(optionalModuleApi)
                .addOnSuccessListener {
                    if (it.areModulesAvailable()) {
                        startQRScanner()
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "QR-Scanner Modul wird intialisiert",
                            Toast.LENGTH_SHORT
                        ).show()
                        val moduleInstallRequest = ModuleInstallRequest.newBuilder()
                            .addApi(GmsBarcodeScanning.getClient(requireContext()))
                            .build()
                        moduleInstallClient
                            .installModules(moduleInstallRequest).addOnSuccessListener {
                                startQRScanner()
                            }.addOnFailureListener {
                                Toast.makeText(
                                    requireContext(),
                                    "QR-Scanner Modul kann nicht initialisiert werden",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                    }
                }
                .addOnFailureListener {
                    Toast.makeText(
                        requireContext(),
                        "QR-Scanner Modul kann nicht initialisiert werden",
                        Toast.LENGTH_SHORT
                    ).show()
                }


        }
    }

    private fun startQRScanner() {
        val options = GmsBarcodeScannerOptions.Builder()
            .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
            .build()
        val scanner = GmsBarcodeScanning.getClient(requireContext(), options)
        scanner.startScan()
            .addOnSuccessListener {
                val data = it.rawValue
                if (data != null) {
                    scannedData = data
                    runnable = Runnable {
                        findNavController().navigate(
                            VerifyScanFragmentDirections.actionVerifyScanFragmentToVerifyDataFragment(
                                scannedData
                            )
                        )
                        runnable = null
                    }
                } else {
                    Toast.makeText(
                        requireContext(),
                        R.string.qr_scan_invalid_data,
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }
            .addOnFailureListener {
                Toast.makeText(
                    requireContext(),
                    R.string.qr_scan_failure,
                    Toast.LENGTH_SHORT
                ).show()
            }
    }

    override fun onResume() {
        super.onResume()
        runnable?.run()
    }
}