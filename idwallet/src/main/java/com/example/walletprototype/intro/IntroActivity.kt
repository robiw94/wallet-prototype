package com.example.walletprototype.intro

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.walletprototype.MainActivity
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.util.PreferenceKeys.INTRO_COMPLETED
import com.github.appintro.AppIntro
import com.github.appintro.AppIntroFragment
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class IntroActivity : AppIntro() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setIndicatorColor(
            selectedIndicatorColor = getColor(R.color.md_theme_light_primary),
            unselectedIndicatorColor = getColor(R.color.md_theme_light_secondary)
        )

        setNextArrowColor(R.color.md_theme_light_primary)
        setSkipTextAppearance(R.style.IntroSkipDoneTextStyle)
        setDoneTextAppearance(R.style.IntroSkipDoneTextStyle)
        setDoneText(R.string.done)
        setSkipText(R.string.skip)

        addSlide(
            AppIntroFragment.createInstance(
                imageDrawable = R.drawable.intro_1,
                title = getString(R.string.intro_1_title),
                description = getString(R.string.intro_1_description),
                backgroundColorRes = R.color.md_theme_light_onPrimary,
                titleColorRes = R.color.md_theme_light_onBackground,
                descriptionColorRes = R.color.md_theme_light_onBackground
            )
        )
        addSlide(
            AppIntroFragment.createInstance(
                imageDrawable = R.drawable.intro_2,
                title = getString(R.string.intro_2_title),
                description = getString(R.string.intro_2_description),
                backgroundColorRes = R.color.md_theme_light_onPrimary,
                titleColorRes = R.color.md_theme_light_onBackground,
                descriptionColorRes = R.color.md_theme_light_onBackground
            )
        )
        addSlide(
            AppIntroFragment.createInstance(
                imageDrawable = R.drawable.intro_3,
                title = getString(R.string.intro_3_title),
                description = getString(R.string.intro_3_description),
                backgroundColorRes = R.color.md_theme_light_onPrimary,
                titleColorRes = R.color.md_theme_light_onBackground,
                descriptionColorRes = R.color.md_theme_light_onBackground
            )
        )
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        runBlocking { setIntroCompleted() }
        val nextActivity = Intent(this, MainActivity::class.java)
        startActivity(nextActivity)
        this.finish()
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        runBlocking { setIntroCompleted() }
        val nextActivity = Intent(this, MainActivity::class.java)
        startActivity(nextActivity)
        this.finish()
    }

    private suspend fun setIntroCompleted() {
        WalletPrototypeApp.preferenceStore.storeBooleanValue(INTRO_COMPLETED, true)
    }
}