package com.example.walletprototype

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.walletprototype.databinding.ActivityMainBinding
import com.example.walletprototype.intro.IntroActivity
import com.example.walletprototype.login.LoginActivity
import com.example.walletprototype.setup.SetupActivity
import com.example.walletprototype.util.PreferenceKeys
import com.google.android.gms.common.moduleinstall.ModuleInstall
import com.google.android.gms.common.moduleinstall.ModuleInstallRequest
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var navController: NavController? = null
    private var menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val moduleInstall = ModuleInstall.getClient(this)
        val moduleInstallRequest = ModuleInstallRequest.newBuilder()
            .addApi(GmsBarcodeScanning.getClient(this))
            .build()
        moduleInstall
            .installModules(moduleInstallRequest)

        val introCompleted =
            runBlocking { WalletPrototypeApp.preferenceStore.readBooleanValue(PreferenceKeys.INTRO_COMPLETED) }
        val setupCompleted =
            runBlocking { WalletPrototypeApp.preferenceStore.readBooleanValue(PreferenceKeys.SETUP_COMPLETED) }

        if (!introCompleted) {
            startActivity(Intent(this, IntroActivity::class.java))
            finish()
            return
        }
        if (!setupCompleted) {
            startActivity(Intent(this, SetupActivity::class.java))
            finish()
            return
        }

        val loggedIn =
            runBlocking {
                WalletPrototypeApp.loginState.isLoggedIn()
            }
        if (!loggedIn) {
            val loginIntent = Intent(this, LoginActivity::class.java)
            if (intent.data?.host == "verify_request") {
                loginIntent.putExtra("intent", intent)
            }
            startActivity(loginIntent)
            finish()
            return
        }

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appBar)
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.fragmentContainerViewMain)
        navController = navHostFragment?.findNavController()
        val appBarConfig = AppBarConfiguration(
            setOf(
                R.id.mainFragment,
                R.id.addIdCardPinFragment,
                R.id.addIdCardDoneFragment,
                R.id.addIdCardTransmitIdFragment
            )
        )
        if (navController != null) {
            setupActionBarWithNavController(navController!!, appBarConfig)
        }


    }

    override fun onResume() {
        super.onResume()
        val loggedIn =
            runBlocking {
                WalletPrototypeApp.loginState.isLoggedIn()
            }
        if (!loggedIn) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
            return
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        this.menu = menu
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressedDispatcher.onBackPressed()
                true
            }

            R.id.action_add -> {
                navController?.navigate(R.id.action_mainFragment_to_addWalletCardFragment)
                return true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    fun setAddMenuItemVisibility(isVisible: Boolean) {
        menu?.findItem(R.id.action_add)?.let { it.isVisible = isVisible }
    }
}
