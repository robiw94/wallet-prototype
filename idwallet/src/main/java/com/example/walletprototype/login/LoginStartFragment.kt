package com.example.walletprototype.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.databinding.FragmentLoginStartBinding
import com.example.walletprototype.setup.SetupActivity
import com.example.walletprototype.util.AuthenticationMethod
import com.example.walletprototype.util.PreferenceKeys
import com.example.walletprototype.util.PreferenceStore
import kotlinx.coroutines.runBlocking

class LoginStartFragment : Fragment() {

    private var _binding: FragmentLoginStartBinding? = null
    private val binding get() = _binding!!

    private lateinit var loginMethod: AuthenticationMethod

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginStartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var authMethodPref: Int?

        runBlocking {
            authMethodPref =
                WalletPrototypeApp.preferenceStore.readValue(PreferenceKeys.AUTHENTICATION_METHOD)
        }

        if(authMethodPref == null) {
            startActivity(Intent(requireContext(), SetupActivity::class.java))
            requireActivity().finish()
            return
        }

        try {
            loginMethod = AuthenticationMethod.convertIntToEnumValue(authMethodPref!!)
        } catch (error: Error) {
            startActivity(Intent(requireContext(), SetupActivity::class.java))
            requireActivity().finish()
            return
        }


        binding.buttonLogin.setOnClickListener {
            val action: NavDirections  = when(loginMethod) {
                AuthenticationMethod.PIN -> LoginStartFragmentDirections.actionLoginStartFragmentToLoginPinFragment()
                AuthenticationMethod.PASSWORD -> LoginStartFragmentDirections.actionLoginStartFragmentToLoginPasswordFragment()
                AuthenticationMethod.FINGERPRINT_PASSWORD -> LoginStartFragmentDirections.actionLoginStartFragmentToLoginFingerprintFragment(AuthenticationMethod.FINGERPRINT_PASSWORD)
                AuthenticationMethod.FINGERPRINT_PIN -> LoginStartFragmentDirections.actionLoginStartFragmentToLoginFingerprintFragment(AuthenticationMethod.FINGERPRINT_PIN)
                AuthenticationMethod.SYSTEM_AUTH -> LoginStartFragmentDirections.actionLoginStartFragmentToLoginDeviceFragment()
            }

            findNavController().navigate(action)
        }


    }

}