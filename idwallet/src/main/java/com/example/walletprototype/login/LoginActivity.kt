package com.example.walletprototype.login

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.walletprototype.R
import com.example.walletprototype.databinding.ActivityLoginBinding
import com.example.walletprototype.databinding.ActivitySetupBinding

class LoginActivity : AppCompatActivity() {
    lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }
}