package com.example.walletprototype.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.MainActivity
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.databinding.FragmentLoginPinBinding
import com.example.walletprototype.util.PreferenceKeys
import kotlinx.coroutines.runBlocking

class LoginPinFragment : Fragment() {
    private var _binding: FragmentLoginPinBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginPinBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.firstPinView.requestFocus()
        val inputMethodManager =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(binding.firstPinView, InputMethodManager.SHOW_IMPLICIT)


        binding.buttonContinue.setOnClickListener {
            val pin =
                runBlocking { WalletPrototypeApp.preferenceStore.readValue(PreferenceKeys.USER_PIN) }
            if(pin == binding.firstPinView.text.toString()) {
                runBlocking { WalletPrototypeApp.loginState.login() }
                var nextActivity = Intent(requireContext(), MainActivity::class.java)
                if (requireActivity().intent.hasExtra("intent")) {
                    nextActivity =
                        requireActivity().intent.getParcelableExtra("intent")!!
                }
                startActivity(nextActivity)
                requireActivity().finish()
            } else {
                binding.firstPinView.setText("")
                binding.firstPinView.setLineColor(
                    resources.getColor(
                        R.color.md_theme_light_error,
                        null
                    )
                )
                Toast.makeText(
                    requireContext(),
                    R.string.pin_not_correct,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        binding.firstPinView.doAfterTextChanged { text ->
            binding.firstPinView.setLineColor(
                resources.getColor(
                    R.color.md_theme_light_primary,
                    null
                )
            )
            binding.buttonContinue.isEnabled = !text.isNullOrBlank()
        }


    }
}