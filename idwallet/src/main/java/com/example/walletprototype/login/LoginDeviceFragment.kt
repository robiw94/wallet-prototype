package com.example.walletprototype.login

import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.MainActivity
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.databinding.FragmentLoginDeviceBinding
import com.example.walletprototype.databinding.FragmentLoginStartBinding
import com.example.walletprototype.util.AuthenticationMethod
import kotlinx.coroutines.runBlocking
import java.util.concurrent.Executor

class LoginDeviceFragment : Fragment() {

    private var _binding: FragmentLoginDeviceBinding? = null
    private val binding get() = _binding!!

    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginDeviceBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

        if (Build.VERSION.SDK_INT >= 30) {
            if (BiometricManager.from(requireContext())
                    .canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.DEVICE_CREDENTIAL) != BiometricManager.BIOMETRIC_SUCCESS
            ) {
                binding.textViewDeviceLogin.text =
                    getString(R.string.device_auth_not_enrolled)
                binding.imageViewFingerprint.setImageResource(R.drawable.baseline_remove_circle_128)
                binding.buttonContinue.text = getString(R.string.fingerprint_setup_now)
                binding.buttonContinue.visibility = View.VISIBLE
                binding.buttonContinue.isEnabled = true
                binding.buttonContinue.setOnClickListener {
                    val enrollIntent = Intent(Settings.ACTION_BIOMETRIC_ENROLL)
                    enrollIntent.putExtra(
                        Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED,
                        BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.DEVICE_CREDENTIAL
                    )
                    startActivity(enrollIntent)
                }
                return
            }

            executor = ContextCompat.getMainExecutor(requireContext())
            biometricPrompt = BiometricPrompt(this, executor,
                object : BiometricPrompt.AuthenticationCallback() {
                    override fun onAuthenticationError(
                        errorCode: Int,
                        errString: CharSequence
                    ) {
                        super.onAuthenticationError(errorCode, errString)
                        binding.textViewDeviceLogin.text =
                            getString(R.string.login_device_auth_failed)
                        val waitTime = 30L
                        binding.textViewRetryCountdown.visibility = View.VISIBLE
                        binding.textViewRetryCountdown.text = waitTime.toString()
                        object : CountDownTimer(waitTime * 1000, 1000) {
                            override fun onTick(millisUntilFinished: Long) {
                                binding.textViewRetryCountdown.text =
                                    (millisUntilFinished / 1000).toString()
                            }

                            override fun onFinish() {
                                binding.buttonContinue.isEnabled = true
                                binding.textViewRetryCountdown.visibility = View.GONE
                            }
                        }.start()
                        binding.imageViewFingerprint.visibility = View.GONE
                        binding.buttonContinue.setOnClickListener {
                            biometricPrompt.authenticate(promptInfo)
                        }
                        binding.buttonContinue.isEnabled = false
                        binding.buttonContinue.text = getString(R.string.retry)
                        binding.buttonContinue.visibility = View.VISIBLE
                    }

                    override fun onAuthenticationSucceeded(
                        result: BiometricPrompt.AuthenticationResult
                    ) {
                        super.onAuthenticationSucceeded(result)
                        runBlocking { WalletPrototypeApp.loginState.login() }
                        var nextActivity = Intent(requireContext(), MainActivity::class.java)
                        if (requireActivity().intent.hasExtra("intent")) {
                            nextActivity =
                                requireActivity().intent.getParcelableExtra("intent")!!
                        }
                        startActivity(nextActivity)
                        requireActivity().finish()
                    }

                    override fun onAuthenticationFailed() {
                        super.onAuthenticationFailed()
                        binding.textViewDeviceLogin.text =
                            getString(R.string.login_fingerprint_failure_try)
                        binding.imageViewFingerprint.visibility = View.VISIBLE
                        binding.imageViewFingerprint.setImageResource(R.drawable.baseline_remove_circle_128)
                    }
                })

            promptInfo = BiometricPrompt.PromptInfo.Builder()
                .setTitle("Login mittels Gerätentsperrmethode")
                .setSubtitle("Login per Gerätentsperrmethode")
                .setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.DEVICE_CREDENTIAL)
                .build()
            biometricPrompt.authenticate(promptInfo)
        } else {
            val keyGuardManager =
                requireContext().getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager

            val intent = keyGuardManager.createConfirmDeviceCredentialIntent(
                "Login mittels Gerätentsperrmethode",
                "Login per Gerätentsperrmethode"
            )
            startActivityForResult(intent, 42)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 42) {
            if (resultCode == Activity.RESULT_OK) {
                runBlocking { WalletPrototypeApp.loginState.login() }
                var nextActivity = Intent(requireContext(), MainActivity::class.java)
                if (requireActivity().intent.hasExtra("intent")) {
                    nextActivity =
                        requireActivity().intent.getParcelableExtra("intent")!!
                }
                startActivity(nextActivity)
                requireActivity().finish()
            } else {
                binding.textViewDeviceLogin.text =
                    getString(R.string.login_device_auth_failed)
                val waitTime = 30L
                binding.textViewRetryCountdown.visibility = View.VISIBLE
                binding.textViewRetryCountdown.text = waitTime.toString()
                object : CountDownTimer(waitTime * 1000, 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                        binding.textViewRetryCountdown.text =
                            (millisUntilFinished / 1000).toString()
                    }

                    override fun onFinish() {
                        binding.buttonContinue.isEnabled = true
                        binding.textViewRetryCountdown.visibility = View.GONE
                    }
                }.start()
                binding.imageViewFingerprint.visibility = View.GONE
                binding.buttonContinue.setOnClickListener {
                    biometricPrompt.authenticate(promptInfo)
                }
                binding.buttonContinue.isEnabled = false
                binding.buttonContinue.text = getString(R.string.retry)
                binding.buttonContinue.visibility = View.VISIBLE
            }
        }
    }
}