package com.example.walletprototype.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.walletprototype.MainActivity
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.databinding.FragmentLoginFingerprintBinding
import com.example.walletprototype.setup.newpassword.SetupFingerprintFragmentArgs
import com.example.walletprototype.util.AuthenticationMethod
import kotlinx.coroutines.runBlocking
import java.util.concurrent.Executor

class LoginFingerprintFragment : Fragment() {

    private var _binding: FragmentLoginFingerprintBinding? = null
    private val binding get() = _binding!!

    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo
    private lateinit var authMethod: AuthenticationMethod

    private val args: SetupFingerprintFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginFingerprintBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        authMethod = args.authMethod

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

        if (BiometricManager.from(requireContext())
                .canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG) != BiometricManager.BIOMETRIC_SUCCESS
        ) {
            binding.textViewFingerprintLogin.text =
                getString(R.string.setup_fingerprint_not_enrolled)
            binding.imageViewFingerprint.setImageResource(R.drawable.baseline_remove_circle_128)
            binding.buttonContinue.text = when (authMethod) {
                AuthenticationMethod.FINGERPRINT_PIN -> getString(R.string.login_with_pin)
                AuthenticationMethod.FINGERPRINT_PASSWORD -> getString(R.string.login_with_password)
                else -> {
                    getString(R.string.string_continue)
                }
            }
            binding.buttonContinue.visibility = View.VISIBLE
            binding.buttonContinue.isEnabled = true
            binding.buttonContinue.setOnClickListener {
                val action = when (authMethod) {
                    AuthenticationMethod.FINGERPRINT_PASSWORD -> LoginFingerprintFragmentDirections.actionLoginFingerprintFragmentToLoginPasswordFragment()

                    AuthenticationMethod.FINGERPRINT_PIN -> LoginFingerprintFragmentDirections.actionLoginFingerprintFragmentToLoginPinFragment()

                    else -> LoginFingerprintFragmentDirections.actionLoginFingerprintFragmentToLoginPasswordFragment()
                }
                findNavController().navigate(action)
            }
        }

        executor = ContextCompat.getMainExecutor(requireContext())
        biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    binding.textViewFingerprintLogin.text =
                        getString(R.string.login_fingerprint_failure)
                    binding.imageViewFingerprint.setImageResource(R.drawable.baseline_remove_circle_128)
                    binding.buttonContinue.text = when (authMethod) {
                        AuthenticationMethod.FINGERPRINT_PIN -> getString(R.string.login_with_pin)
                        AuthenticationMethod.FINGERPRINT_PASSWORD -> getString(R.string.login_with_password)
                        else -> {
                            getString(R.string.string_continue)
                        }
                    }
                    binding.buttonContinue.visibility = View.VISIBLE
                    binding.buttonContinue.isEnabled = true
                    binding.buttonContinue.setOnClickListener {
                        val action = when (authMethod) {
                            AuthenticationMethod.FINGERPRINT_PASSWORD -> LoginFingerprintFragmentDirections.actionLoginFingerprintFragmentToLoginPasswordFragment()

                            AuthenticationMethod.FINGERPRINT_PIN -> LoginFingerprintFragmentDirections.actionLoginFingerprintFragmentToLoginPinFragment()

                            else -> LoginFingerprintFragmentDirections.actionLoginFingerprintFragmentToLoginPasswordFragment()
                        }
                        findNavController().navigate(action)
                    }
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)
                    runBlocking { WalletPrototypeApp.loginState.login() }
                    var nextActivity = Intent(requireContext(), MainActivity::class.java)
                    if (requireActivity().intent.hasExtra("intent")) {
                        nextActivity =
                            requireActivity().intent.getParcelableExtra("intent")!!
                    }
                    startActivity(nextActivity)
                    requireActivity().finish()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    binding.textViewFingerprintLogin.text =
                        getString(R.string.login_fingerprint_failure_try)
                    binding.imageViewFingerprint.visibility = View.VISIBLE
                    binding.imageViewFingerprint.setImageResource(R.drawable.baseline_remove_circle_128)
                }
            })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Login per Fingerabdruck")
            .setSubtitle("Nutzen sie ihren Fingerabdruch zum Login")
            .setNegativeButtonText("Abbrechen")
            .setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG)
            .build()
        biometricPrompt.authenticate(promptInfo)


    }
}