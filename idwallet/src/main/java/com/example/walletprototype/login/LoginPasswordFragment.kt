package com.example.walletprototype.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.MainActivity
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.databinding.FragmentLoginFingerprintBinding
import com.example.walletprototype.databinding.FragmentLoginPasswordBinding
import com.example.walletprototype.setup.util.SetupStage
import com.example.walletprototype.util.PreferenceKeys
import kotlinx.coroutines.runBlocking

class LoginPasswordFragment : Fragment() {
    private var _binding: FragmentLoginPasswordBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginPasswordBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonCancelPassword.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.editTextPasswordLogin.editText?.requestFocus()
        val inputMethodManager =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(
            binding.editTextPasswordLogin.editText,
            InputMethodManager.SHOW_IMPLICIT
        )

        binding.buttonConfirmPassword.setOnClickListener {

            val password =
                runBlocking { WalletPrototypeApp.preferenceStore.readValue(PreferenceKeys.USER_PASSWORD) }

            if (binding.editTextPasswordLogin.editText?.text.toString() == password) {
                runBlocking { WalletPrototypeApp.loginState.login() }
                var nextActivity = Intent(requireContext(), MainActivity::class.java)
                if (requireActivity().intent.hasExtra("intent")) {
                    nextActivity =
                        requireActivity().intent.getParcelableExtra("intent")!!
                }
                startActivity(nextActivity)
                requireActivity().finish()
            } else {
                binding.editTextPasswordLogin.boxStrokeColor =
                    resources.getColor(
                        R.color.md_theme_light_error,
                        null
                    )

                Toast.makeText(
                    requireContext(),
                    R.string.password_not_correct,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        binding.editTextPasswordLogin.editText?.doAfterTextChanged { text ->
            binding.editTextPasswordLogin.boxStrokeColor =
                resources.getColor(
                    R.color.md_theme_light_primary,
                    null
                )
            binding.buttonConfirmPassword.isEnabled =
                !text.isNullOrBlank()
        }
    }
}