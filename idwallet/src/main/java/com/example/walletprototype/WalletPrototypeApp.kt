package com.example.walletprototype

import android.app.Application
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import androidx.room.Room
import com.example.walletprototype.db.WalletCardsDatabase
import com.example.walletprototype.service.WalletCardsService
import com.example.walletprototype.util.LoginState
import com.example.walletprototype.util.PreferenceStore

class WalletPrototypeApp : Application() {
    fun getContext(): Context {
        return this
    }

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

    companion object {
        lateinit var preferenceStore: PreferenceStore
        lateinit var loginState: LoginState
        lateinit var database: WalletCardsDatabase
        lateinit var walletCardsService: WalletCardsService
    }

    override fun onCreate() {
        super.onCreate()
        preferenceStore = PreferenceStore(applicationContext.dataStore)
        loginState = LoginState(preferenceStore)
        database = Room.databaseBuilder(
            this,
            WalletCardsDatabase::class.java, "wallet-cards"
        ).build()
        walletCardsService = WalletCardsService(database.walletCardDao())
    }
}