package com.example.walletprototype.util

import androidx.room.TypeConverter
import com.example.walletprototype.data.IdCardData
import java.time.OffsetDateTime

class Converters {
    @TypeConverter
    fun fromIsoString(value: String?): OffsetDateTime? {
        return value?.let { OffsetDateTime.parse(it) }
    }

    @TypeConverter
    fun offsetDateTimeToIsoString(date: OffsetDateTime?): String? {
        return date?.toString()
    }
}