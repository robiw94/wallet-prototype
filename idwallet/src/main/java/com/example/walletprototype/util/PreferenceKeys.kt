package com.example.walletprototype.util

import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey

object PreferenceKeys {
    val INTRO_COMPLETED = booleanPreferencesKey("intro_completed")
    val SETUP_COMPLETED = booleanPreferencesKey("setup_completed")
    val AUTHENTICATION_METHOD = intPreferencesKey("authentication_method")
    val USER_PASSWORD = stringPreferencesKey("user_password")
    val USER_PIN = stringPreferencesKey("user_pin")
    val LAST_LOGIN = stringPreferencesKey("last_login")
}