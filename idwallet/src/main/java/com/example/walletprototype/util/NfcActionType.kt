package com.example.walletprototype.util

enum class NfcActionType {
    READ_ID_CARD,
    TRANSMIT_ID_CARD
}