package com.example.walletprototype.util

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class PreferenceStore(private val dataStore: DataStore<Preferences>) {

    suspend fun storeBooleanValue(key: Preferences.Key<Boolean>, value: Boolean) {
        dataStore.edit {
            it[key] = value
        }
    }

    suspend fun readBooleanValue(key: Preferences.Key<Boolean>): Boolean {
        return dataStore.data.map { prefs -> prefs[key] ?: false }.first()
    }

    suspend fun <T> storeValue(key: Preferences.Key<T>, value: T) {
        dataStore.edit {
            it[key] = value
        }
    }

    suspend fun <T> readValue(key: Preferences.Key<T>): T? {
        return dataStore.data.map { prefs -> prefs[key] }.first()
    }

}