package com.example.walletprototype.util

enum class AuthenticationMethod {
    PIN,
    PASSWORD,
    FINGERPRINT_PIN,
    FINGERPRINT_PASSWORD,
    SYSTEM_AUTH;

    companion object {
        fun convertIntToEnumValue(value: Int): AuthenticationMethod {
            return AuthenticationMethod.values()
                .first { authenticationMethod -> authenticationMethod.ordinal == value }
        }
    }
}

