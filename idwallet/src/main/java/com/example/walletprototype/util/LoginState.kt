package com.example.walletprototype.util

import java.time.Duration
import java.time.OffsetDateTime

class LoginState(private val prefStore: PreferenceStore) {

    suspend fun isLoggedIn(): Boolean {
        val lastLogin = prefStore.readValue(PreferenceKeys.LAST_LOGIN) ?: return false
        val lastLoginDate = OffsetDateTime.parse(lastLogin)
        val diff = Duration.between(lastLoginDate, OffsetDateTime.now())
        return diff.toMinutes() < 10
    }

    suspend fun login() {
        prefStore.storeValue(PreferenceKeys.LAST_LOGIN, OffsetDateTime.now().toString())
    }

}