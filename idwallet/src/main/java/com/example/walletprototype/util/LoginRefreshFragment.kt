package com.example.walletprototype.util

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.walletprototype.WalletPrototypeApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

open class LoginRefreshFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CoroutineScope(Dispatchers.IO).launch {  WalletPrototypeApp.loginState.login() }
    }
}