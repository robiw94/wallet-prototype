package com.example.walletprototype.data

enum class IdentityType(val title: String) {
    ID_CARD( "Personalausweis"),
    DRIVERS_LICENSE("Führerschein"),
    HEALTH_CARD("Gesundheitskarte"),
    OWN_DATA("Meine Daten"),
    OTHER_CARD("Karte"),
    DIGITAL_KEY("Schlüssel")
}