package com.example.walletprototype.data

data class VerifyDataItem(val title: String, val value: String) {

    fun getCompareValue(): Int {
        return when (this.title) {
            "Vorname" -> 1
            "Nachname" -> 2
            "Geburtsname" -> 3
            "Geburtsdatum" -> 4
            "Geburtsort" -> 5
            "Addresse" -> 6
            "Nationalität" -> 7
            "Ausweisnummer" -> 8
            "Ausstellerstaat" -> 9
            "Ablaufdatum" -> 10
            else -> Int.MAX_VALUE
        }
    }

}
