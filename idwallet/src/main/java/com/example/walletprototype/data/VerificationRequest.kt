package com.example.walletprototype.data

import java.io.Serializable

data class VerificationRequest(
    val provider: ProviderData,
    val requestedFields: Array<String>
) : Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as VerificationRequest

        if (provider != other.provider) return false
        if (!requestedFields.contentEquals(other.requestedFields)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = provider.hashCode()
        result = 31 * result + requestedFields.contentHashCode()
        return result
    }
}

data class ProviderData(
    val name: String,
    val serviceName: String,
    val certificate: String,
    val information: String,
    val usage: String
) : Serializable
