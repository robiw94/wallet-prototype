package com.example.walletprototype.data

import com.example.walletprototype.R
import java.time.OffsetDateTime

fun walletCards(): List<WalletCard> {
    return listOf(
        WalletCard("Max Mustermann", IdentityType.ID_CARD, OffsetDateTime.now().plusYears(1L), R.drawable.ic_brd),
        WalletCard("Max Mustermann", IdentityType.DRIVERS_LICENSE, OffsetDateTime.now().plusYears(1L), null),
        WalletCard("Max Mustermann", IdentityType.HEALTH_CARD, OffsetDateTime.now().plusYears(1L), null),
    )
}

fun otherCards(): List<WalletCard> {
    return listOf(
        WalletCard("Max Mustermann", IdentityType.OWN_DATA, null, null),
        WalletCard("Max Mustermann", IdentityType.OTHER_CARD, OffsetDateTime.now().plusYears(1L), null),
    )
}

fun digitalKeys(): List<WalletCard> {
    return listOf(
        WalletCard("Adlon Hotel Berlin", IdentityType.DIGITAL_KEY, OffsetDateTime.now().plusDays(5L), R.drawable.baseline_key_48),
        WalletCard("Jaguar F-Type", IdentityType.DIGITAL_KEY, null, R.drawable.baseline_key_48),
    )
}