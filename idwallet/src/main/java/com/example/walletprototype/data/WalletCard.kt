package com.example.walletprototype.data

import androidx.annotation.DrawableRes
import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.OffsetDateTime

@Entity
data class WalletCard(
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "identity_type") val identityType: IdentityType,
    @ColumnInfo(name = "expiry_date") val expiryDate: OffsetDateTime?,
    @ColumnInfo(name = "icon_res_id") @DrawableRes val icon: Int?,
    @Embedded(prefix = "id_card_data_") var idCardData: IdCardData? = null,
    @Embedded(prefix = "my_data_") var myData: MyData? = null,
    @PrimaryKey(autoGenerate = true) val uid: Int = 0,
) {

}

data class IdCardData (
    val firstName: String,
    val lastname: String,
    val birthName: String,
    val birthDate: String,
    val birthPlace: String,
    val address: String,
    val nationality: String,
    val idCardNr: String,
    val issuingState: String,
    val photo: String? = null
)

data class MyData(
    var mobileNumber: String,
    var telephoneNumber: String,
    var privateMail: String,
    var businessMail: String
)
