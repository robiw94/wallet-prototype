package com.example.walletprototype.data

import java.time.format.DateTimeFormatter
import kotlin.reflect.full.memberProperties

data class VerifyData(
    val firstName: String?,
    val lastname: String?,
    val birthName: String?,
    val birthDate: String?,
    val birthPlace: String?,
    val address: String?,
    val nationality: String?,
    val idCardNr: String?,
    val issuingState: String?,
    val expirationDate: String?,
    val withPhoto: Boolean = true
) {

    fun toVerifyDataItemList(): List<VerifyDataItem> {
        return VerifyData::class.memberProperties.map { member -> VerifyDataItem(titleForVerifyDataProperty(member.name), member.get(this).toString()) }.sortedBy { it.getCompareValue() }
    }
    companion object {
        fun fromWalletCard(
            card: WalletCard,
            includedFields: List<String>? = null
        ): VerifyData {

            if (includedFields == null) {
                return VerifyData(
                    firstName = card.idCardData?.firstName,
                    lastname = card.idCardData?.lastname,
                    birthName = card.idCardData?.birthName,
                    birthDate = card.idCardData?.birthDate,
                    birthPlace = card.idCardData?.birthPlace,
                    address = card.idCardData?.address,
                    nationality = card.idCardData?.nationality,
                    idCardNr = card.idCardData?.idCardNr,
                    issuingState = card.idCardData?.issuingState,
                    expirationDate = card.expiryDate?.format(
                        DateTimeFormatter.ofPattern("dd.MM.yyyy")
                    ),

                )
            } else {
                return VerifyData(
                    firstName = if (includedFields.contains("firstName")) card.idCardData?.firstName else null,
                    lastname = if (includedFields.contains("lastName")) card.idCardData?.lastname else null,
                    birthName = if (includedFields.contains("birthName")) card.idCardData?.birthName else null,
                    birthDate = if (includedFields.contains("birthDate")) card.idCardData?.birthDate else null,
                    birthPlace = if (includedFields.contains("birthPlace")) card.idCardData?.birthPlace else null,
                    address = if (includedFields.contains("address")) card.idCardData?.address else null,
                    nationality = if (includedFields.contains("nationality")) card.idCardData?.nationality else null,
                    idCardNr = if (includedFields.contains("idCardNr")) card.idCardData?.idCardNr else null,
                    issuingState = if (includedFields.contains("issuingState")) card.idCardData?.issuingState else null,
                    expirationDate = if (includedFields.contains("expirationDate")) card.expiryDate?.format(
                        DateTimeFormatter.ofPattern("dd.MM.yyyy")
                    ) else null,
                    withPhoto = includedFields.contains("photo")
                )
            }
        }

        fun titleForVerifyDataProperty(propertyName: String): String {
            return when(propertyName){
                "firstName" -> "Vorname"
                "lastName" -> "Familienname"
                "birthName" -> "Geburtsname"
                "birthDate" -> "Geburtsdatum"
                "birthPlace" -> "Geburtsort"
                "address" -> "Addresse"
                "nationality" -> "Nationalität"
                "idCardNr" -> "Ausweisnummer"
                "issuingState" -> "Ausstellerstaat"
                "expirationDate" -> "Ablaufdatum"
                "photo" -> "Ausweisfoto"
                else -> "Unbekannt"
            }
        }
    }
}
