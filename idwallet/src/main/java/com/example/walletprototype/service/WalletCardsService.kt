package com.example.walletprototype.service

import com.example.walletprototype.dao.WalletCardDao
import com.example.walletprototype.data.IdentityType
import com.example.walletprototype.data.WalletCard

class WalletCardsService(val walletCardDao: WalletCardDao) {

    suspend fun getAllWalletCards(): List<WalletCard> {
        return this.walletCardDao.getAllWalletCards()
    }

    suspend fun getWalletCardByIdentityType(identityType: IdentityType): WalletCard {
        return this.walletCardDao.findWalletCardByIdentityType(identityType)
    }

    suspend fun getWalletCardsByIdentityTypes(identityTypes: List<IdentityType>): List<WalletCard> {
        return this.walletCardDao.findWalletCardsByIdentityTypes(identityTypes)
    }

    suspend fun addWalletCard(card: WalletCard) {
        this.walletCardDao.insertWalletCards(card)
    }

    suspend fun updateWalletCard(card: WalletCard) {
        this.walletCardDao.updateWalletCards(card)
    }

    suspend fun deleteWalletCard(card: WalletCard) {
        this.walletCardDao.deleteWalletCard(card)
    }


}