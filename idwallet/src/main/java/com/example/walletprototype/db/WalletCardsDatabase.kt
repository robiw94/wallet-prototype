package com.example.walletprototype.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.walletprototype.dao.WalletCardDao
import com.example.walletprototype.data.WalletCard
import com.example.walletprototype.util.Converters

@Database(entities = [WalletCard::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class WalletCardsDatabase : RoomDatabase() {
    abstract fun walletCardDao(): WalletCardDao
}