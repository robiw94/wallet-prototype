package com.example.walletprototype.ui.walletcards.addIdCard

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.databinding.FragmentAddIdCardPinBinding
import com.example.walletprototype.util.LoginRefreshFragment

class AddIdCardPinFragment : LoginRefreshFragment() {

    private var _binding: FragmentAddIdCardPinBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddIdCardPinBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.firstPinView.requestFocus()
        val inputMethodManager =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(binding.firstPinView, InputMethodManager.SHOW_IMPLICIT)

        binding.buttonContinue.setOnClickListener {
            findNavController().navigate(R.id.action_addIdCardPinFragment_to_addIdCardTransmitIdFragment)
        }

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.firstPinView.setOnEditorActionListener { _, _, _ ->
            if (binding.buttonContinue.isEnabled) {
                findNavController().navigate(R.id.action_addIdCardPinFragment_to_addIdCardTransmitIdFragment)
                return@setOnEditorActionListener true
            } else {
                Toast.makeText(
                    requireContext(),
                    R.string.enter_6_chars,
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnEditorActionListener false
            }
        }

        binding.firstPinView.doAfterTextChanged { text ->
            binding.buttonContinue.isEnabled = !text.isNullOrBlank() && text.length == 6
        }
    }
}