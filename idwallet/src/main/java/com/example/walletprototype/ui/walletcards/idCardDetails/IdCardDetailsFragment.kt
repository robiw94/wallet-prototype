package com.example.walletprototype.ui.walletcards.idCardDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.data.IdentityType
import com.example.walletprototype.data.WalletCard
import com.example.walletprototype.databinding.FragmentIdCardDetailsBinding
import com.example.walletprototype.util.LoginRefreshFragment
import kotlinx.coroutines.runBlocking
import java.time.format.DateTimeFormatter

class IdCardDetailsFragment : LoginRefreshFragment() {
    private var _binding: FragmentIdCardDetailsBinding? = null
    private val binding get() = _binding!!

    private var card: WalletCard? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        card = runBlocking {
            WalletPrototypeApp.walletCardsService.getWalletCardByIdentityType(IdentityType.ID_CARD)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentIdCardDetailsBinding.inflate(inflater, container, false)
        card?.let { card ->
            card.idCardData?.let {
                binding.textViewIdDetailsFamilyName.text = it.lastname
                binding.textViewIdDetailsBirthName.text = it.birthName
                binding.textViewIdDetailsFirstName.text = it.firstName
                binding.textViewIdDetailsAddress.text = it.address
                binding.textViewIdDetailsBirthDate.text = it.birthDate
                binding.textViewIdDetailsBirthPlace.text = it.birthPlace
                binding.textViewIdDetailsNationality.text = it.nationality
                binding.textViewIdDetailsIdCardNr.text = it.idCardNr
                binding.textViewIdDetailsIssuingState.text = it.issuingState
                binding.textViewIdDetailsValidity.text =
                    card.expiryDate?.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))
                        ?: "01.01.1970"
            }

        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

  //      requireActivity().onBackPressedDispatcher.addCallback {
  //          if(isEnabled){
  //              isEnabled = false
  //              findNavController().popBackStack()
  //          }
  //      }

        binding.fabOptionValidateLocally.setOnClickListener {
            findNavController().navigate(R.id.action_idCardDetailsFragment_to_idCardQrCodeFragment)
        }

        binding.fabOptionValidateLocallySelective.setOnClickListener {
            findNavController().navigate(R.id.action_idCardDetailsFragment_to_idCardDetailsSelectiveFragment)
        }

    }
}