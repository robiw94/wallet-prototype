package com.example.walletprototype.ui.mydata

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.data.IdentityType
import com.example.walletprototype.data.MyData
import com.example.walletprototype.data.WalletCard
import com.example.walletprototype.databinding.FragmentMyDataBinding
import kotlinx.coroutines.runBlocking

class MyDataFragment : Fragment() {
    private var _binding: FragmentMyDataBinding? = null
    private val binding get() = _binding!!

    private lateinit var myDataCard: WalletCard

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        myDataCard = runBlocking {
            WalletPrototypeApp.walletCardsService.getWalletCardByIdentityType(IdentityType.OWN_DATA)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentMyDataBinding.inflate(inflater, container, false)

        if (myDataCard.myData == null) {
            myDataCard.myData = MyData("", "", "", "")
        }

        binding.editTextMailPrivate.setText(myDataCard.myData!!.privateMail)
        binding.editTextMailBusiness.setText(myDataCard.myData!!.businessMail)
        binding.editTextPhonePrivate.setText(myDataCard.myData!!.mobileNumber)
        binding.editTextPhoneBusiness.setText(myDataCard.myData!!.telephoneNumber)

        binding.imageViewEditMailPrivate.setOnClickListener {
            if (binding.editTextMailPrivate.isEnabled) {
                binding.imageViewEditMailPrivate.setImageResource(R.drawable.baseline_edit_24)
                binding.editTextMailPrivate.isEnabled = false
                binding.imageViewCancelMailPrivate.visibility = View.GONE
                myDataCard.myData!!.privateMail = binding.editTextMailPrivate.text.toString()
                saveMyData()
            } else {
                binding.imageViewEditMailPrivate.setImageResource(R.drawable.baseline_check_24)
                binding.imageViewCancelMailPrivate.visibility = View.VISIBLE
                binding.imageViewCancelMailPrivate.setOnClickListener {
                    binding.editTextMailPrivate.setText(myDataCard.myData!!.privateMail)
                    binding.imageViewEditMailPrivate.setImageResource(R.drawable.baseline_edit_24)
                    binding.editTextMailPrivate.isEnabled = false
                    binding.imageViewCancelMailPrivate.visibility = View.GONE
                }
                binding.editTextMailPrivate.isEnabled = true
                binding.editTextMailPrivate.setSelection(binding.editTextMailPrivate.length())
                binding.editTextMailPrivate.requestFocus()
                val inputMethodManager =
                    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.showSoftInput(
                    binding.editTextMailPrivate,
                    InputMethodManager.SHOW_IMPLICIT
                )
            }
        }

        binding.imageViewEditMailBusiness.setOnClickListener {
            if (binding.editTextMailBusiness.isEnabled) {
                binding.imageViewEditMailBusiness.setImageResource(R.drawable.baseline_edit_24)
                binding.editTextMailBusiness.isEnabled = false
                binding.imageViewCancelMailBusiness.visibility = View.GONE
                myDataCard.myData!!.businessMail = binding.editTextMailBusiness.text.toString()
                saveMyData()
            } else {
                binding.imageViewEditMailBusiness.setImageResource(R.drawable.baseline_check_24)
                binding.imageViewCancelMailBusiness.visibility = View.VISIBLE
                binding.imageViewCancelMailBusiness.setOnClickListener {
                    binding.editTextMailBusiness.setText(myDataCard.myData!!.businessMail)
                    binding.imageViewEditMailBusiness.setImageResource(R.drawable.baseline_edit_24)
                    binding.editTextMailBusiness.isEnabled = false
                    binding.imageViewCancelMailBusiness.visibility = View.GONE
                }
                binding.editTextMailBusiness.isEnabled = true
                binding.editTextMailBusiness.setSelection(binding.editTextMailBusiness.length())
                binding.editTextMailBusiness.requestFocus()
                val inputMethodManager =
                    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.showSoftInput(
                    binding.editTextMailBusiness,
                    InputMethodManager.SHOW_IMPLICIT
                )
            }
        }

        binding.imageViewEditPhonePrivate.setOnClickListener {
            if (binding.editTextPhonePrivate.isEnabled) {
                binding.imageViewEditPhonePrivate.setImageResource(R.drawable.baseline_edit_24)
                binding.editTextPhonePrivate.isEnabled = false
                binding.imageViewCancelPhonePrivate.visibility = View.GONE
                myDataCard.myData!!.mobileNumber = binding.editTextPhonePrivate.text.toString()
                saveMyData()
            } else {
                binding.imageViewEditPhonePrivate.setImageResource(R.drawable.baseline_check_24)
                binding.imageViewCancelPhonePrivate.visibility = View.VISIBLE
                binding.imageViewCancelPhonePrivate.setOnClickListener {
                    binding.editTextPhonePrivate.setText(myDataCard.myData!!.mobileNumber)
                    binding.imageViewEditPhonePrivate.setImageResource(R.drawable.baseline_edit_24)
                    binding.editTextPhonePrivate.isEnabled = false
                    binding.imageViewCancelPhonePrivate.visibility = View.GONE
                }
                binding.editTextPhonePrivate.isEnabled = true
                binding.editTextPhonePrivate.setSelection(binding.editTextPhonePrivate.length())
                binding.editTextPhonePrivate.requestFocus()
                val inputMethodManager =
                    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.showSoftInput(
                    binding.editTextPhonePrivate,
                    InputMethodManager.SHOW_IMPLICIT
                )
            }
        }

        binding.imageViewEditPhoneBusiness.setOnClickListener {
            if (binding.editTextPhoneBusiness.isEnabled) {
                binding.imageViewEditPhoneBusiness.setImageResource(R.drawable.baseline_edit_24)
                binding.editTextPhoneBusiness.isEnabled = false
                binding.imageViewCancelPhoneBusiness.visibility = View.GONE
                myDataCard.myData!!.telephoneNumber = binding.editTextPhoneBusiness.text.toString()
                saveMyData()

            } else {
                binding.imageViewEditPhoneBusiness.setImageResource(R.drawable.baseline_check_24)
                binding.imageViewCancelPhoneBusiness.visibility = View.VISIBLE
                binding.imageViewCancelPhoneBusiness.setOnClickListener { 
                    binding.editTextPhoneBusiness.setText(myDataCard.myData!!.telephoneNumber)
                    binding.imageViewEditPhoneBusiness.setImageResource(R.drawable.baseline_edit_24)
                    binding.editTextPhoneBusiness.isEnabled = false
                    binding.imageViewCancelPhoneBusiness.visibility = View.GONE
                }
                binding.editTextPhoneBusiness.isEnabled = true
                binding.editTextPhoneBusiness.setSelection(binding.editTextPhoneBusiness.length())
                binding.editTextPhoneBusiness.requestFocus()
                val inputMethodManager =
                    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.showSoftInput(
                    binding.editTextPhoneBusiness,
                    InputMethodManager.SHOW_IMPLICIT
                )
            }
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.imageViewCancelMailPrivate.visibility = View.GONE
        binding.imageViewCancelMailBusiness.visibility = View.GONE
        binding.imageViewCancelPhonePrivate.visibility = View.GONE
        binding.imageViewCancelPhoneBusiness.visibility = View.GONE

        requireActivity().onBackPressedDispatcher.addCallback {
            if (isEnabled) {
                isEnabled = false
                findNavController().popBackStack()
            }
        }

    }

    private fun saveMyData() {
        runBlocking {
            WalletPrototypeApp.walletCardsService.updateWalletCard(myDataCard)
        }
    }
}