package com.example.walletprototype.ui.walletcards.idCardDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.data.IdentityType
import com.example.walletprototype.data.VerifyData
import com.example.walletprototype.databinding.FragmentIdCardQrCodeBinding
import com.example.walletprototype.util.LoginRefreshFragment
import com.google.gson.Gson
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.coroutines.runBlocking

class IdCardQrCodeFragment : LoginRefreshFragment() {
    private var _binding: FragmentIdCardQrCodeBinding? = null
    private val binding get() = _binding!!

    private lateinit var verifyData: VerifyData

    private val args: IdCardQrCodeFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val card = runBlocking {
            WalletPrototypeApp.walletCardsService.getWalletCardByIdentityType(IdentityType.ID_CARD)
        }
        card?.let {
            val includedFields =
                if (args.includedFields != null) args.includedFields!!.toList() else null
            verifyData = VerifyData.fromWalletCard(card, includedFields)
        }


    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentIdCardQrCodeBinding.inflate(inflater, container, false)

        val qrCodeBitmap = BarcodeEncoder().encodeBitmap(
            Gson().toJson(verifyData), BarcodeFormat.QR_CODE, 400, 400,
            mapOf(Pair(EncodeHintType.CHARACTER_SET, "UTF-8"), Pair(EncodeHintType.MARGIN, 0))
        )

        val data = if (args.includedFields != null) args.includedFields!!.toList() else listOf(
            "photo",
            "firstName",
            "lastName",
            "birthName",
            "birthDate",
            "birthPlace",
            "address",
            "nationality",
            "idCardNr",
            "issuingState",
            "expirationDate"
        )

        binding.requestItemsList.adapter = QRCodeShareItemsAdapter(data.toTypedArray())

        binding.imageViewQRCode.setImageBitmap(qrCodeBitmap)


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonBackToWallet.setOnClickListener {
            findNavController().navigate(R.id.action_idCardQrCodeFragment_to_mainFragment)
        }
    }
}