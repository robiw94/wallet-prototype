package com.example.walletprototype.ui.walletcards.addIdCard

import android.nfc.NfcAdapter
import android.nfc.Tag
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.databinding.FragmentAddIdCardReadIdBinding
import com.example.walletprototype.util.LoginRefreshFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AddIdCardReadIdFragment : NfcAdapter.ReaderCallback, LoginRefreshFragment() {
    private var _binding: FragmentAddIdCardReadIdBinding? = null
    private val binding get() = _binding!!

    private var nfcAdapter: NfcAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        nfcAdapter = NfcAdapter.getDefaultAdapter(requireContext())
        if (nfcAdapter == null) {
            return
        }
    }

    override fun onPause() {
        super.onPause()
        nfcAdapter?.disableReaderMode(requireActivity())
    }

    override fun onResume() {
        super.onResume()
        if (nfcAdapter == null) {
            nfcAdapter = NfcAdapter.getDefaultAdapter(requireContext())?: return
        }
        readNfc()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddIdCardReadIdBinding.inflate(inflater, container, false)
        if (nfcAdapter == null) {
            binding.textViewNoNfc.visibility = View.VISIBLE
            binding.imageView.setImageResource(R.drawable.baseline_remove_circle_128)
        }

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

        return binding.root
    }

    private fun readNfc() {
        nfcAdapter?.enableReaderMode(requireActivity(), this, NfcAdapter.FLAG_READER_NFC_A or NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK, null)
    }

    override fun onTagDiscovered(tag: Tag?) {
        lifecycleScope.launch { handleNfcRead() }

    }

    private suspend fun handleNfcRead(){
        withContext(Dispatchers.Main) {
            findNavController().navigate(R.id.action_addIdCardReadIdFragment_to_addIdCardPinFragment)
        }
    }
}