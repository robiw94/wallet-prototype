package com.example.walletprototype.ui.walletcards.idCardDetails.request

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.walletprototype.data.VerificationRequest
import com.example.walletprototype.databinding.FragmentIdCardDetailsRequestBinding
import com.example.walletprototype.util.LoginRefreshFragment
import com.google.gson.Gson

class IdCardDetailsRequestFragment : LoginRefreshFragment() {
    private var _binding: FragmentIdCardDetailsRequestBinding? = null
    private val binding get() = _binding!!

    private val args: IdCardDetailsRequestFragmentArgs by navArgs()

    private lateinit var requestData: VerificationRequest

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        if(args.requestData != null) {
            requestData = args.requestData!!
        } else if (args.requestDataString != null){
            requestData = Gson().fromJson(args.requestDataString!!, VerificationRequest::class.java)
        }


        _binding = FragmentIdCardDetailsRequestBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.requestItemsList.adapter = RequestItemsAdapter(requestData.requestedFields)

        binding.textViewRequestService.text = requestData.provider.serviceName
        binding.textViewRequestUsage.text = requestData.provider.usage

        binding.imageViewInfo.setOnClickListener {
            val action = IdCardDetailsRequestFragmentDirections.actionIdCardDetailsRequestFragmentToProviderDetailsFragment(requestData.provider)
            findNavController().navigate(action)
        }


        binding.buttonContinue.setOnClickListener {
            val action =
                IdCardDetailsRequestFragmentDirections.actionIdCardDetailsRequestFragmentToIdCardQrCodeFragment(
                    requestData.requestedFields
                )
            findNavController().navigate(action)
        }

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

  //      requireActivity().onBackPressedDispatcher.addCallback {
  //          if(isEnabled){
  //              isEnabled = false
  //              findNavController().popBackStack()
  //          }
  //      }
    }
}