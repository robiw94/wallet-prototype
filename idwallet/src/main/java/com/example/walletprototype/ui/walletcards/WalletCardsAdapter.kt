package com.example.walletprototype.ui.walletcards

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.walletprototype.R
import com.example.walletprototype.data.IdentityType
import com.example.walletprototype.data.WalletCard
import java.time.format.DateTimeFormatter

class WalletCardsAdapter(
    private val data: List<WalletCard>,
    private val onClickListener: WalletOnClickListener
) :
    RecyclerView.Adapter<WalletCardsAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewTitle: TextView
        val textViewExpiration: TextView
        val textViewIdentityType: TextView
        val icon: ImageView
        val cardView: CardView
        val iconQrCode: ImageView

        init {
            textViewTitle = view.findViewById(R.id.textViewTitle)
            textViewExpiration = view.findViewById(R.id.textViewExpiry)
            textViewIdentityType = view.findViewById(R.id.TextViewIdentityType)
            icon = view.findViewById(R.id.imageViewIcon)
            cardView = view.findViewById(R.id.WalletCard)
            iconQrCode = view.findViewById(R.id.imageViewQRCode)
        }

    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.wallet_card_item, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.textViewTitle.text =
            if (data[position].identityType != IdentityType.OWN_DATA) data[position].title else ""

        data[position].expiryDate?.let {
            viewHolder.textViewExpiration.text = viewHolder.textViewExpiration.context.getString(
                R.string.expiration,
                it.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))
            )
        }

        viewHolder.textViewIdentityType.text = data[position].identityType.title
        val icon = when (data[position].identityType) {
            IdentityType.ID_CARD -> R.drawable.ic_brd
            IdentityType.OWN_DATA -> R.drawable.baseline_person_24
            IdentityType.DIGITAL_KEY -> R.drawable.baseline_key_48
            else -> R.drawable.id_card_solid_black
        }
        viewHolder.icon.setImageResource(icon)

        if (data[position].identityType == IdentityType.ID_CARD) {
            viewHolder.iconQrCode.visibility = View.VISIBLE
            viewHolder.iconQrCode.setOnClickListener {
                onClickListener.onQrClick()
            }
            viewHolder.cardView.setOnClickListener {
                onClickListener.onClick()
            }
        }
        if (data[position].identityType == IdentityType.OWN_DATA) {
            viewHolder.cardView.setOnClickListener {
                onClickListener.onMyDataClick()
            }
        }
    }

    override fun getItemCount() = data.size

    class WalletOnClickListener(
        val clickListener: () -> Unit,
        val qrClickListener: () -> Unit,
        val myDataClickListener: () -> Unit
    ) {
        fun onClick() = clickListener()

        fun onQrClick() = qrClickListener()

        fun onMyDataClick() = myDataClickListener()
    }

}