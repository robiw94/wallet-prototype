package com.example.walletprototype.ui.walletcards.idCardDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.view.children
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.data.IdentityType
import com.example.walletprototype.data.WalletCard
import com.example.walletprototype.databinding.FragmentIdCardDetailsSelectiveBinding
import com.example.walletprototype.util.LoginRefreshFragment
import com.google.android.material.checkbox.MaterialCheckBox
import kotlinx.coroutines.runBlocking

class IdCardDetailsSelectiveFragment : LoginRefreshFragment() {
    private var _binding: FragmentIdCardDetailsSelectiveBinding? = null
    private val binding get() = _binding!!

    private var card: WalletCard? = null

    private var includedFields: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        card = runBlocking {
            WalletPrototypeApp.walletCardsService.getWalletCardByIdentityType(IdentityType.ID_CARD)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentIdCardDetailsSelectiveBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

  //      requireActivity().onBackPressedDispatcher.addCallback {
  //          if(isEnabled){
  //              isEnabled = false
  //              findNavController().popBackStack()
  //          }
  //      }

        binding.buttonCancelSelective.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.buttonContinueSelective.setOnClickListener {
            includedFields.clear()

            val checkboxes = (view as ViewGroup).children.toList().filterIsInstance<MaterialCheckBox>()
            checkboxes.forEach {
                if (it.isChecked) {
                    includedFields.add(it.tag.toString())
                }
            }

            val action =
                IdCardDetailsSelectiveFragmentDirections.actionIdCardDetailsSelectiveFragmentToIdCardQrCodeFragment(
                    includedFields.toTypedArray()
                )
            findNavController().navigate(action)
        }


    }
}