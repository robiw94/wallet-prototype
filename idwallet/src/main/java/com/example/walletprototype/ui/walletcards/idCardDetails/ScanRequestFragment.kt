package com.example.walletprototype.ui.walletcards.idCardDetails

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.data.VerificationRequest
import com.example.walletprototype.databinding.FragmentScanRequestBinding
import com.google.android.gms.common.moduleinstall.ModuleInstall
import com.google.android.gms.common.moduleinstall.ModuleInstallRequest
import com.google.gson.Gson
import com.google.mlkit.vision.barcode.common.Barcode
import com.google.mlkit.vision.codescanner.GmsBarcodeScannerOptions
import com.google.mlkit.vision.codescanner.GmsBarcodeScanning

class ScanRequestFragment : Fragment() {
    private var _binding: FragmentScanRequestBinding? = null
    private val binding get() = _binding!!

    private lateinit var scannedData: VerificationRequest
    private var runnable: Runnable? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentScanRequestBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

  //      requireActivity().onBackPressedDispatcher.addCallback {
  //          if(isEnabled){
  //              isEnabled = false
  //              findNavController().popBackStack()
  //          }
  //      }

        binding.buttonStartScan.setOnClickListener {
            val moduleInstallClient = ModuleInstall.getClient(requireContext())
            val optionalModuleApi = GmsBarcodeScanning.getClient(requireContext())
            moduleInstallClient
                .areModulesAvailable(optionalModuleApi)
                .addOnSuccessListener {
                    if (it.areModulesAvailable()) {
                        startQRScanner()
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "QR-Scanner Modul wird intialisiert",
                            Toast.LENGTH_SHORT
                        ).show()
                        val moduleInstallRequest = ModuleInstallRequest.newBuilder()
                            .addApi(GmsBarcodeScanning.getClient(requireContext()))
                            .build()
                        moduleInstallClient
                            .installModules(moduleInstallRequest).addOnSuccessListener {
                                startQRScanner()
                            }.addOnFailureListener {
                                Toast.makeText(
                                    requireContext(),
                                    "QR-Scanner Modul kann nicht initialisiert werden",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                    }
                }
                .addOnFailureListener {
                    Toast.makeText(
                        requireContext(),
                        "QR-Scanner Modul kann nicht initialisiert werden",
                        Toast.LENGTH_SHORT
                    ).show()
                }
        }
    }

    private fun startQRScanner() {
        val options = GmsBarcodeScannerOptions.Builder()
            .setBarcodeFormats(Barcode.FORMAT_QR_CODE)
            .build()
        val scanner = GmsBarcodeScanning.getClient(requireContext(), options)
        scanner.startScan()
            .addOnSuccessListener {
                val rawValue = Uri.decode(it.rawValue?.replace("walletprototype://verify_request/", ""))
                val data = Gson().fromJson(rawValue, VerificationRequest::class.java)
                if (data != null) {
                    scannedData = data
                    runnable = Runnable {
                        findNavController().navigate(
                            ScanRequestFragmentDirections.actionScanRequestFragmentToIdCardDetailsRequestFragment(
                                requestData = scannedData
                            )
                        )
                        runnable = null
                    }
                } else {
                    Toast.makeText(
                        requireContext(),
                        R.string.qr_scan_invalid_data,
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }
            .addOnFailureListener {
                Toast.makeText(
                    requireContext(),
                    R.string.qr_scan_failure,
                    Toast.LENGTH_SHORT
                ).show()
            }
    }

    override fun onResume() {
        super.onResume()
        runnable?.run()
    }
}