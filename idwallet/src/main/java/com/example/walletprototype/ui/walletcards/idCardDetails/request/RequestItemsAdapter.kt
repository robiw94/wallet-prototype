package com.example.walletprototype.ui.walletcards.idCardDetails.request

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import com.example.walletprototype.R

class RequestItemsAdapter(private val data: Array<String>) :
    RecyclerView.Adapter<RequestItemsAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewValue: TextView


        init {
            textViewValue = view.findViewById(R.id.textViewIdVerifyRequestValue)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.id_verify_request_item, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textViewValue.text =
            holder.textViewValue.context.getString(getStringResourceForField(data[position]))
    }

    @StringRes
    private fun getStringResourceForField(field: String): Int {
        return when (field) {
            "firstName" -> R.string.first_name
            "lastName" -> R.string.family_name
            "birthName" -> R.string.birth_name
            "birthDate" -> R.string.birth_date
            "address" -> R.string.address
            "birthPlace" -> R.string.birth_place
            "validity" -> R.string.validity
            "idCardNr" -> R.string.id_card_nr
            "nationality" -> R.string.nationality
            "issuingState" -> R.string.issuing_state
            "photo" -> R.string.photo
            else -> R.string.not_found
        }
    }
}