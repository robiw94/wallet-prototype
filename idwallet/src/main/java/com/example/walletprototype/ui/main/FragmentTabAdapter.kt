package com.example.walletprototype.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.walletprototype.data.IdentityType
import com.example.walletprototype.ui.walletcards.WalletCardsFragment


class FragmentTabAdapter(activity: FragmentActivity) : FragmentStateAdapter(activity) {


    override fun getItemCount(): Int {
        return 3
    }

    override fun createFragment(position: Int): Fragment {

        return when (position) {
            0 -> WalletCardsFragment(
                listOf(IdentityType.ID_CARD, IdentityType.DRIVERS_LICENSE, IdentityType.HEALTH_CARD)
            )

            1 -> WalletCardsFragment(listOf(IdentityType.OTHER_CARD, IdentityType.OWN_DATA))
            2 -> WalletCardsFragment(listOf(IdentityType.DIGITAL_KEY))
            else -> WalletCardsFragment(
                listOf(
                    IdentityType.ID_CARD,
                    IdentityType.DRIVERS_LICENSE,
                    IdentityType.HEALTH_CARD
                )
            )
        }

    }
}