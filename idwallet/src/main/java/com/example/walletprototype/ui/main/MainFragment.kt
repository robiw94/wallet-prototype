package com.example.walletprototype.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.example.walletprototype.MainActivity
import com.example.walletprototype.R
import com.example.walletprototype.databinding.FragmentMainBinding
import com.example.walletprototype.util.LoginRefreshFragment
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainFragment : LoginRefreshFragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentMainBinding.inflate(inflater, container, false)

        (requireActivity() as MainActivity).setAddMenuItemVisibility(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sectionsPagerAdapter = FragmentTabAdapter(requireActivity())
        val viewPager: ViewPager2 = binding.viewPager
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = binding.tabs
        TabLayoutMediator(tabs, viewPager) { tab, position ->

            tab.text = when(position) {
                0 -> "Hoheitliche Karten"
                1 -> "Andere Karten"
                2 -> "Schlüssel"
                else -> ""
            }
        }.attach()



        binding.fab.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_scanRequestFragment)
        }
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).setAddMenuItemVisibility(true)
    }

    override fun onStart() {
        super.onStart()
        (requireActivity() as MainActivity).setAddMenuItemVisibility(true)
    }

    override fun onPause() {
        super.onPause()
        (requireActivity() as MainActivity).setAddMenuItemVisibility(false)
    }

    override fun onStop() {
        super.onStop()
        (requireActivity() as MainActivity).setAddMenuItemVisibility(false)

    }
}