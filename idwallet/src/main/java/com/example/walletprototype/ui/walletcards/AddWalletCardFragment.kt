package com.example.walletprototype.ui.walletcards

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.data.IdentityType
import com.example.walletprototype.databinding.FragmentAddWalletCardBinding
import kotlinx.coroutines.runBlocking

class AddWalletCardFragment : Fragment() {
    private var _binding: FragmentAddWalletCardBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddWalletCardBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        runBlocking {
            if(WalletPrototypeApp.walletCardsService.getWalletCardByIdentityType(IdentityType.ID_CARD) != null){
                binding.setupWalletCardId.setCardBackgroundColor(Color.parseColor("#A5A5A5"))
                binding.setupWalletCardId.isEnabled = false
                binding.textViewAlreadyAdded.visibility = View.VISIBLE
            }
        }

        binding.setupWalletCardId.setOnClickListener {
            findNavController().navigate(R.id.action_addWalletCardFragment_to_addIdCardPreparationFragment)
        }

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

  //      requireActivity().onBackPressedDispatcher.addCallback {
  //          if(isEnabled){
  //              isEnabled = false
  //              findNavController().popBackStack()
  //          }
  //      }
    }
}