package com.example.walletprototype.ui.walletcards.addIdCard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.data.IdCardData
import com.example.walletprototype.data.IdentityType
import com.example.walletprototype.data.WalletCard
import com.example.walletprototype.databinding.FragmentAddIdCardDoneBinding
import com.example.walletprototype.util.LoginRefreshFragment
import kotlinx.coroutines.launch
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Month
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class AddIdCardDoneFragment : LoginRefreshFragment() {
    private var _binding: FragmentAddIdCardDoneBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launch {
            WalletPrototypeApp.walletCardsService.addWalletCard(
                WalletCard(
                    title = "Hans-Günther Dr.eh.Dr. von Drebenbusch Dalgoßen",
                    IdentityType.ID_CARD,
                    OffsetDateTime.of(LocalDateTime.of(2029,Month.NOVEMBER,30,0,0), ZoneOffset.UTC),
                    null,
                    IdCardData(
                        "Hans-Günther",
                        "Dr.eh.Dr. von Drebenbusch Dalgoßen",
                        "Weiß",
                        "25.01.1946",
                        "Bremerhaven",
                        "Weg Nr.12 8E, 22043 Hamburg",
                        "Deutsch",
                        "T020CGGW0",
                        "Deutschland")
                )
            )
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddIdCardDoneBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonContinue.setOnClickListener {
            findNavController().navigate(R.id.action_addIdCardDoneFragment_to_mainFragment)
        }
    }
}