package com.example.walletprototype.ui.walletcards

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.data.IdentityType
import com.example.walletprototype.data.WalletCard
import com.example.walletprototype.data.digitalKeys
import com.example.walletprototype.data.otherCards
import com.example.walletprototype.data.walletCards
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class WalletCardFragmentViewModel(private val identityTypes: List<IdentityType>) : ViewModel() {

    val cards: MutableLiveData<List<WalletCard>> by lazy {
        MutableLiveData()
    }

    fun initDemoData() {
        runBlocking {
            if (WalletPrototypeApp.walletCardsService.getAllWalletCards().isEmpty()) {
                WalletPrototypeApp.walletCardsService.walletCardDao.insertWalletCards(
                    *walletCards().toTypedArray(),
                    *otherCards().toTypedArray(),
                    *digitalKeys().toTypedArray()
                )
            }
        }

    }

    fun initMyDataCard() {
        runBlocking {
            if (WalletPrototypeApp.walletCardsService.getWalletCardByIdentityType(IdentityType.OWN_DATA) == null) {
                WalletPrototypeApp.walletCardsService.walletCardDao.insertWalletCards(
                    WalletCard("Max Mustermann", IdentityType.OWN_DATA, null, null),
                )
            }
        }
    }

    fun fetchAllCardsForFragment() {
        viewModelScope.launch {
            cards.value =
                WalletPrototypeApp.walletCardsService.getWalletCardsByIdentityTypes(identityTypes)
        }

    }

    fun deleteCard(card: WalletCard) {
        viewModelScope.launch {
            WalletPrototypeApp.walletCardsService.deleteWalletCard(card)
            cards.value =
                WalletPrototypeApp.walletCardsService.getWalletCardsByIdentityTypes(identityTypes)
        }
    }
}