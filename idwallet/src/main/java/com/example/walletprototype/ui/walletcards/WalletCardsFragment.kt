package com.example.walletprototype.ui.walletcards

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.walletprototype.R
import com.example.walletprototype.data.IdentityType
import com.example.walletprototype.databinding.FragmentWalletCardsBinding
import com.example.walletprototype.util.LoginRefreshFragment


class WalletCardsFragment(private val identityTypes: List<IdentityType>) : LoginRefreshFragment() {

    private val walletCardFragmentViewModel: WalletCardFragmentViewModel by viewModels {
        WalletCardFragmentViewModelFactory(
            identityTypes
        )
    }
    private var _binding: FragmentWalletCardsBinding? = null
    private val binding get() = _binding!!

    private lateinit var walletCardsItemsAdapter: WalletCardsAdapter
    private lateinit var walletCardsLayoutManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        walletCardFragmentViewModel.initMyDataCard()
        walletCardFragmentViewModel.fetchAllCardsForFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentWalletCardsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        walletCardFragmentViewModel.cards.observe(viewLifecycleOwner) {

            if (it.isEmpty()) {
                if(identityTypes == listOf(IdentityType.DIGITAL_KEY)) {
                    binding.emptyListViewKeys.visibility = View.VISIBLE
                } else {
                    binding.emptyListView.visibility = View.VISIBLE
                    binding.buttonAddId.setOnClickListener {
                        findNavController().navigate(R.id.action_mainFragment_to_addWalletCardFragment)
                    }
                }
                binding.walletCardList.visibility = View.GONE
            } else {
                binding.emptyListView.visibility = View.GONE
                binding.walletCardList.visibility = View.VISIBLE
                walletCardsLayoutManager = LinearLayoutManager(this.context)
                walletCardsItemsAdapter =
                    WalletCardsAdapter(
                        it,
                        WalletCardsAdapter.WalletOnClickListener(
                            { findNavController().navigate(R.id.action_mainFragment_to_idCardDetailsFragment) },
                            { findNavController().navigate(R.id.action_mainFragment_to_idCardQrCodeFragment) },
                            { findNavController().navigate(R.id.action_mainFragment_to_myDataFragment)})
                    )
                binding.walletCardList.apply {
                    layoutManager = walletCardsLayoutManager
                    adapter = walletCardsItemsAdapter
                }
            }
        }


    }
}