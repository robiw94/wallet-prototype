package com.example.walletprototype.ui.walletcards

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.walletprototype.data.IdentityType

class WalletCardFragmentViewModelFactory(private val identityTypes: List<IdentityType>): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T = WalletCardFragmentViewModel(identityTypes) as T

}