package com.example.walletprototype.ui.walletcards.idCardDetails.request

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.walletprototype.databinding.FragmentProviderDetailsBinding
import com.example.walletprototype.util.LoginRefreshFragment

class ProviderDetailsFragment : LoginRefreshFragment() {
    private var _binding: FragmentProviderDetailsBinding? = null
    private val binding get() = _binding!!

    private val args: ProviderDetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProviderDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.textViewProviderService.text = args.providerData.serviceName
        binding.textViewProviderName.text = args.providerData.name
        binding.textViewProviderCert.text = args.providerData.certificate
        binding.textViewProviderInfo.text = args.providerData.information
        binding.textViewProviderUsage.text = args.providerData.usage


        binding.buttonBack.setOnClickListener {
            findNavController().popBackStack()
        }


    }
}