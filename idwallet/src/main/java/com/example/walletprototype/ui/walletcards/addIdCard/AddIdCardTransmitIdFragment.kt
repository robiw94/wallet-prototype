package com.example.walletprototype.ui.walletcards.addIdCard

import android.app.PendingIntent
import android.content.Intent
import android.content.IntentFilter
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.nfc.tech.NfcA
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.databinding.FragmentAddIdCardTransmitIdBinding
import com.example.walletprototype.util.NfcActionType
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AddIdCardTransmitIdFragment : NfcAdapter.ReaderCallback, Fragment() {
    private var _binding: FragmentAddIdCardTransmitIdBinding? = null
    private val binding get() = _binding!!

    private var nfcAdapter: NfcAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        nfcAdapter = NfcAdapter.getDefaultAdapter(requireContext())
        if (nfcAdapter == null) {
            return
        }
    }

    override fun onPause() {
        super.onPause()
        nfcAdapter?.disableForegroundDispatch(requireActivity())
    }

    override fun onResume() {
        super.onResume()
        nfcAdapter = NfcAdapter.getDefaultAdapter(requireContext())
        if (nfcAdapter == null) {
            return
        }
        readNfc()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddIdCardTransmitIdBinding.inflate(inflater, container, false)

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

        return binding.root
    }

    private fun readNfc() {
        nfcAdapter?.enableReaderMode(requireActivity(), this, NfcAdapter.FLAG_READER_NFC_A or NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK, null)
    }

    override fun onTagDiscovered(tag: Tag?) {
        lifecycleScope.launch { handleNfcRead() }

    }

    private suspend fun handleNfcRead(){
        withContext(Dispatchers.Main) {
            binding.progressBar.visibility = View.VISIBLE
            binding.imageView.visibility = View.GONE
            binding.textViewIdAddTransmitDesc.text =
                getString(R.string.desc_transmit_id_card_in_progress)
        }
        object : CountDownTimer(4000, 40) {
            override fun onTick(millisUntilFinished: Long) {
                binding.progressBar.incrementProgressBy(1)
            }

            override fun onFinish() {
                findNavController().navigate(R.id.action_addIdCardTransmitIdFragment_to_addIdCardDoneFragment)
            }
        }.start()
    }
}