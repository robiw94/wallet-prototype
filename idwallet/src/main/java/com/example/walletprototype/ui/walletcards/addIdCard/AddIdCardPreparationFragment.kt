package com.example.walletprototype.ui.walletcards.addIdCard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.databinding.FragmentAddIdCardPreparationBinding
import com.example.walletprototype.setup.dialog.InformationDialogFragment
import com.example.walletprototype.util.LoginRefreshFragment

class AddIdCardPreparationFragment : LoginRefreshFragment() {

    private var _binding: FragmentAddIdCardPreparationBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddIdCardPreparationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonContinue.setOnClickListener {
            findNavController().navigate(R.id.action_addIdCardPreparationFragment_to_addIdCardDataOverviewFragment)
        }

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

  //      requireActivity().onBackPressedDispatcher.addCallback {
  //          if(isEnabled){
  //              isEnabled = false
  //              findNavController().popBackStack()
  //          }
  //      }

        binding.imageButtonNoNewIdCard.setOnClickListener {
            val dialogFragment = InformationDialogFragment(
                description = R.string.prep_information_dialog_new_id_card,
                image = R.drawable.prep_1
            )
            dialogFragment.show(parentFragmentManager, "info")
        }

        binding.imageButtonOnlineNotActivated.setOnClickListener {
            val dialogFragment = InformationDialogFragment(
                description = R.string.prep_information_dialog_online_not_activated,
                image = R.drawable.prep_2
            )
            dialogFragment.show(parentFragmentManager, "info")
        }

        binding.imageButtonUnknownPin.setOnClickListener {
            val dialogFragment = InformationDialogFragment(
                description = R.string.prep_information_dialog_pin_unknown,
                image = R.drawable.prep_3
            )
            dialogFragment.show(parentFragmentManager, "info")
        }
    }
}