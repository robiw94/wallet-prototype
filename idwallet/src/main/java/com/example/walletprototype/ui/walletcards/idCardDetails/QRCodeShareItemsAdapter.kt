package com.example.walletprototype.ui.walletcards.idCardDetails

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.walletprototype.R
import com.example.walletprototype.data.VerifyData.Companion.titleForVerifyDataProperty

class QRCodeShareItemsAdapter(private val data: Array<String>) :
    RecyclerView.Adapter<QRCodeShareItemsAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textViewValue: TextView


        init {
            textViewValue = view.findViewById(R.id.textViewIdVerifyRequestValue)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.id_qr_code_shared_item, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textViewValue.text = titleForVerifyDataProperty(data[position])
    }
}