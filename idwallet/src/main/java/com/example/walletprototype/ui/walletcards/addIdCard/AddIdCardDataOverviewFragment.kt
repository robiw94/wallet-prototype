package com.example.walletprototype.ui.walletcards.addIdCard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.databinding.FragmentAddIdCardDataOverviewBinding
import com.example.walletprototype.util.LoginRefreshFragment

class AddIdCardDataOverviewFragment : LoginRefreshFragment() {
    private var _binding: FragmentAddIdCardDataOverviewBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAddIdCardDataOverviewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonContinue.setOnClickListener {
            findNavController().navigate(R.id.action_addIdCardDataOverviewFragment_to_addIdCardReadIdFragment)
        }

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

  //      requireActivity().onBackPressedDispatcher.addCallback {
  //          if(isEnabled){
  //              isEnabled = false
  //              findNavController().popBackStack()
  //          }
  //      }
    }
}