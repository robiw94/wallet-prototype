package com.example.walletprototype.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.walletprototype.data.IdentityType
import com.example.walletprototype.data.WalletCard

@Dao
interface WalletCardDao {
    @Query("SELECT * FROM walletcard")
    suspend fun getAllWalletCards(): List<WalletCard>

    @Query("SELECT * FROM walletcard WHERE identity_type LIKE :identityType LIMIT 1")
    suspend fun findWalletCardByIdentityType(identityType: IdentityType): WalletCard

    @Query("SELECT * FROM walletcard WHERE identity_type IN (:identityTypes) ")
    suspend fun findWalletCardsByIdentityTypes(identityTypes: List<IdentityType>): List<WalletCard>

    @Insert
    suspend fun insertWalletCards(vararg cards: WalletCard)

    @Update
    suspend fun updateWalletCards(vararg cards: WalletCard)

    @Delete
    suspend fun deleteWalletCard(card: WalletCard)
}