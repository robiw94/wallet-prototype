package com.example.walletprototype.setup.util

enum class SetupStage {
    INITIAL,
    CONFIRM
}