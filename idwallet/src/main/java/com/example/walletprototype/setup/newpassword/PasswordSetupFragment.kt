package com.example.walletprototype.setup.newpassword

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.graphics.toColorInt
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.databinding.FragmentPasswordSetupBinding
import com.example.walletprototype.setup.util.SetupStage
import com.example.walletprototype.util.AuthenticationMethod
import com.example.walletprototype.util.PreferenceKeys
import kotlinx.coroutines.runBlocking

class PasswordSetupFragment : Fragment() {

    private var _binding: FragmentPasswordSetupBinding? = null
    private val binding get() = _binding!!
    private var stage: SetupStage = SetupStage.INITIAL
    private lateinit var password: String
    private val args: PasswordSetupFragmentArgs by navArgs()
    private lateinit var authMethod: AuthenticationMethod

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPasswordSetupBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonCancelPassword.setOnClickListener {
            findNavController().popBackStack()
        }

        authMethod = args.authMethod

        binding.editTextPasswordSetup.editText?.requestFocus()
        val inputMethodManager =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(
            binding.editTextPasswordSetup.editText,
            InputMethodManager.SHOW_IMPLICIT
        )

        binding.buttonConfirmPassword.setOnClickListener {
            handleContinue()
        }

        binding.editTextPasswordSetup.editText?.setOnEditorActionListener { _, _, _ ->
            if (binding.buttonConfirmPassword.isEnabled) {
                handleContinue()
                return@setOnEditorActionListener true
            } else {
                if(stage == SetupStage.INITIAL) {
                    Toast.makeText(
                        requireContext(),
                        R.string.enter_valid_password,
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Toast.makeText(
                        requireContext(),
                        R.string.enter_password,
                        Toast.LENGTH_SHORT
                    ).show()
                }
                return@setOnEditorActionListener false
            }
        }

        binding.editTextPasswordSetup.editText?.doAfterTextChanged { text ->
            binding.editTextPasswordSetup.boxStrokeColor =
                resources.getColor(
                    R.color.md_theme_light_primary,
                    null
                )
            binding.checkBox10Chars.isChecked = is10CharactersLong(text.toString())
            binding.checkBoxNumber.isChecked = hasNumber(text.toString())
            binding.checkBoxSpecialCharacter.isChecked = hasSpecialCharacter(text.toString())
            binding.checkBoxUppercaseAndLowercase.isChecked =
                hasUppercaseAndLowerCase(text.toString())
            binding.checkBoxUppercaseAndLowercase.isChecked =
                hasUppercaseAndLowerCase(text.toString())
            if (isValidPassword(text.toString())) binding.textViewPasswordCriteriaDesc.setTextColor(
                "#009D0F".toColorInt()
            ) else binding.textViewPasswordCriteriaDesc.setTextColor("#1A1C18".toColorInt())
            binding.buttonConfirmPassword.isEnabled =
                !text.isNullOrBlank() && (stage == SetupStage.CONFIRM || isValidPassword(text.toString()))
        }

    }

    private fun hasNumber(pw: String): Boolean {
        val regex = Regex("^(?=.*\\d).{1,}$")
        return regex.matches(pw)
    }

    private fun hasSpecialCharacter(pw: String): Boolean {
        val regex = Regex("^(?=.*[*.!@#\$%^&(){}\\[\\]:;<>,?/~_+\\-=|]).{1,}$")
        return regex.matches(pw)
    }

    private fun hasUppercaseAndLowerCase(pw: String): Boolean {
        val regex = Regex("^(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{1,}$")
        return regex.matches(pw)
    }

    private fun is10CharactersLong(pw: String): Boolean {
        return pw.length >= 10
    }

    private fun isValidPassword(pw: String): Boolean {
        val regex =
            Regex("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z])(?=.*[*.!@#$%^&(){}\\[\\]:;<>,?/~_+\\-=|]).{10,}$")
        return regex.matches(pw)
    }

    private suspend fun setSetupCompleted() {
        WalletPrototypeApp.preferenceStore.storeValue(PreferenceKeys.SETUP_COMPLETED, true)
        WalletPrototypeApp.preferenceStore.storeValue(
            PreferenceKeys.AUTHENTICATION_METHOD,
            authMethod.ordinal
        )
        WalletPrototypeApp.preferenceStore.storeValue(PreferenceKeys.USER_PASSWORD, password)
    }

    fun handleContinue() {
        when (stage) {
            SetupStage.INITIAL -> {
                password = binding.editTextPasswordSetup.editText?.text.toString()
                binding.editTextPasswordSetup.editText?.setText("")
                binding.TextViewPasswordSetupDescription.text =
                    getString(R.string.confirm_password)
                binding.buttonConfirmPassword.text = getString(R.string.confirm)
                binding.buttonConfirmPassword.isEnabled = false
                binding.passwordCriteriaContainer.visibility = View.GONE
                stage = SetupStage.CONFIRM
            }

            SetupStage.CONFIRM -> {
                if (binding.editTextPasswordSetup.editText?.text.toString() == password) {
                    runBlocking { setSetupCompleted() }
                    findNavController().navigate(R.id.action_passwordSetupFragment_to_setupDoneFragment)
                } else {
                    binding.editTextPasswordSetup.boxStrokeColor =
                        resources.getColor(
                            R.color.md_theme_light_error,
                            null
                        )

                    Toast.makeText(
                        requireContext(),
                        R.string.password_not_matching,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
}