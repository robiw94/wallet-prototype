package com.example.walletprototype.setup.newpassword

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.databinding.FragmentPinSetupBinding
import com.example.walletprototype.setup.util.SetupStage
import com.example.walletprototype.util.AuthenticationMethod
import com.example.walletprototype.util.PreferenceKeys
import kotlinx.coroutines.runBlocking


class PinSetupFragment : Fragment() {
    private var _binding: FragmentPinSetupBinding? = null
    private val binding get() = _binding!!
    lateinit var pin: String
    var stage: SetupStage = SetupStage.INITIAL
    private val args: PasswordSetupFragmentArgs by navArgs()
    private lateinit var authMethod: AuthenticationMethod

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPinSetupBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

        authMethod = args.authMethod

        binding.firstPinView.requestFocus()
        val inputMethodManager =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(binding.firstPinView, InputMethodManager.SHOW_IMPLICIT)

        binding.buttonContinue.setOnClickListener {
            handleNext()
        }

        binding.firstPinView.setOnEditorActionListener { _, _, _ ->
            if (binding.buttonContinue.isEnabled) {
                handleNext()
                return@setOnEditorActionListener true
            } else {
                Toast.makeText(
                    requireContext(),
                    R.string.enter_6_chars,
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnEditorActionListener false
            }

        }

        binding.firstPinView.doAfterTextChanged { text ->
            binding.firstPinView.setLineColor(
                resources.getColor(
                    R.color.md_theme_light_primary,
                    null
                )
            )
            binding.buttonContinue.isEnabled = !text.isNullOrBlank() && text.length == 6
        }
    }

    private suspend fun setSetupCompleted() {
        WalletPrototypeApp.preferenceStore.storeValue(PreferenceKeys.SETUP_COMPLETED, true)
        WalletPrototypeApp.preferenceStore.storeValue(
            PreferenceKeys.AUTHENTICATION_METHOD,
            authMethod.ordinal
        )
        WalletPrototypeApp.preferenceStore.storeValue(PreferenceKeys.USER_PIN, pin)
    }

    private fun handleNext() {
        when (stage) {
            SetupStage.INITIAL -> {
                pin = binding.firstPinView.text.toString()
                binding.firstPinView.setText("")
                binding.textViewPinSetupDescription.text = getString(R.string.confirm_pin)
                binding.buttonContinue.text = getString(R.string.confirm)
                binding.buttonContinue.isEnabled = false
                stage = SetupStage.CONFIRM
                binding.firstPinView.requestFocus()
                val inputMethodManager =
                    requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.showSoftInput(
                    binding.firstPinView,
                    InputMethodManager.SHOW_IMPLICIT
                )
            }

            SetupStage.CONFIRM -> {
                if (binding.firstPinView.text.toString() == pin) {
                    runBlocking { setSetupCompleted() }
                    findNavController().navigate(R.id.action_pinSetupFragment_to_setupDoneFragment)
                } else {
                    binding.firstPinView.setText("")
                    binding.firstPinView.setLineColor(
                        resources.getColor(
                            R.color.md_theme_light_error,
                            null
                        )
                    )
                    Toast.makeText(
                        requireContext(),
                        R.string.pin_not_matching,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

        }
    }
}