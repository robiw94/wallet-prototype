package com.example.walletprototype.setup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.databinding.FragmentSetupStartBinding
import com.example.walletprototype.setup.dialog.InformationDialogFragment

class SetupStartFragment : Fragment() {

    private var _binding: FragmentSetupStartBinding? = null
    private val binding get() = _binding!!

    override fun onResume() {
        super.onResume()
        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(false)
    }

    override fun onStop() {
        super.onStop()
        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSetupStartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.ibLoginFromDevice.setOnClickListener {
            val dialogFragment = InformationDialogFragment(
                description = R.string.setup_information_dialog_login_from_device,
                image = R.drawable.setup_1
            )
            dialogFragment.show(parentFragmentManager, "info")
        }

        binding.ibNewPassword.setOnClickListener {
            val dialogFragment = InformationDialogFragment(
                description = R.string.setup_information_dialog_new_password,
                image = R.drawable.setup_2
            )
            dialogFragment.show(parentFragmentManager, "info")
        }

        binding.setupCardNewPassword.setOnClickListener {
            findNavController().navigate(R.id.action_setupStartFragment_to_setupNewPasswordFragment)
        }

        binding.setupCardLoginFromDevice.setOnClickListener {
            findNavController().navigate(R.id.action_setupStartFragment_to_deviceAuthFragment)
        }
    }
}