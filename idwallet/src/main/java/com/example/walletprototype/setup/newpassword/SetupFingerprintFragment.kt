package com.example.walletprototype.setup.newpassword

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.walletprototype.R
import com.example.walletprototype.databinding.FragmentSetupFingerprintBinding
import com.example.walletprototype.util.AuthenticationMethod
import java.util.concurrent.Executor


class SetupFingerprintFragment : Fragment() {

    private var _binding: FragmentSetupFingerprintBinding? = null
    private val binding get() = _binding!!

    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo
    private lateinit var authMethod: AuthenticationMethod
    private var isViewSetup = true


    private val args: SetupFingerprintFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSetupFingerprintBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        if (!isViewSetup) {
            handleBiometricAuth()
        }
        isViewSetup = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isViewSetup = true

        authMethod = args.authMethod

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

        binding.textViewLoginMethod.text = when (authMethod) {
            AuthenticationMethod.FINGERPRINT_PIN -> getString(R.string.setup_new_password_fingerprint_and_pin)
            AuthenticationMethod.FINGERPRINT_PASSWORD -> getString(R.string.setup_new_password_fingerprint_and_password)
            else -> getString(R.string.setup_new_password_fingerprint_and_pin)
        }

        handleBiometricAuth()

    }

    private fun handleBiometricAuth() {
        if (BiometricManager.from(requireContext())
                .canAuthenticate(BIOMETRIC_STRONG) != BiometricManager.BIOMETRIC_SUCCESS
        ) {
            binding.textViewFingerprintSetup.text =
                getString(R.string.setup_fingerprint_not_enrolled)
            binding.imageViewFingerprint.setImageResource(R.drawable.baseline_remove_circle_128)
            binding.buttonContinue.text = getString(R.string.fingerprint_setup_now)
            binding.buttonContinue.visibility = View.VISIBLE
            binding.buttonContinue.isEnabled = true
            binding.buttonContinue.setOnClickListener {
                val enrollIntent = Intent(Settings.ACTION_BIOMETRIC_ENROLL)
                enrollIntent.putExtra(
                    Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED,
                    BIOMETRIC_STRONG
                )
                startActivity(enrollIntent)
            }
            return
        } else {
            binding.textViewFingerprintSetup.text =
                getString(R.string.device_auth_description)
            binding.imageViewFingerprint.setImageResource(R.drawable.outline_fingerprint_128)
            binding.buttonContinue.visibility = View.GONE
        }

        executor = ContextCompat.getMainExecutor(requireContext())
        biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    binding.textViewFingerprintSetup.text =
                        getString(R.string.setup_fingerprint_failure_wait)
                    val waitTime = 30L
                    binding.textViewRetryCountdown.visibility = View.VISIBLE
                    binding.textViewRetryCountdown.text = waitTime.toString()
                    object : CountDownTimer(waitTime * 1000, 1000) {
                        override fun onTick(millisUntilFinished: Long) {
                            binding.textViewRetryCountdown.text =
                                (millisUntilFinished / 1000).toString()
                        }

                        override fun onFinish() {
                            binding.buttonContinue.isEnabled = true
                            binding.textViewRetryCountdown.visibility = View.GONE
                        }
                    }.start()
                    binding.imageViewFingerprint.visibility = View.GONE
                    binding.buttonContinue.setOnClickListener {
                        biometricPrompt.authenticate(promptInfo)
                    }
                    binding.buttonContinue.isEnabled = false
                    binding.buttonContinue.text = getString(R.string.retry)
                    binding.buttonContinue.visibility = View.VISIBLE
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)
                    binding.textViewFingerprintSetup.text =
                        getString(R.string.setup_fingerprint_success)
                    binding.imageViewFingerprint.visibility = View.VISIBLE
                    binding.imageViewFingerprint.setImageResource(R.drawable.baseline_check_circle_128)
                    binding.buttonContinue.setOnClickListener {
                        val action = when (authMethod) {
                            AuthenticationMethod.FINGERPRINT_PASSWORD -> SetupFingerprintFragmentDirections.actionSetupFingerprintFragmentToPasswordSetupFragment(
                                authMethod
                            )

                            AuthenticationMethod.FINGERPRINT_PIN -> SetupFingerprintFragmentDirections.actionSetupFingerprintFragmentToPinSetupFragment(
                                authMethod
                            )

                            else -> SetupFingerprintFragmentDirections.actionSetupFingerprintFragmentToPasswordSetupFragment(
                                authMethod
                            )

                        }
                        findNavController().navigate(action)

                    }
                    binding.buttonContinue.text = getString(R.string.string_continue)
                    binding.buttonContinue.visibility = View.VISIBLE
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    binding.textViewFingerprintSetup.text =
                        getString(R.string.setup_fingerprint_failure)
                    binding.imageViewFingerprint.visibility = View.VISIBLE
                    binding.imageViewFingerprint.setImageResource(R.drawable.baseline_remove_circle_128)
                }
            })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Fingerabdruck Einrichten")
            .setSubtitle("Login per Fingerabdruck")
            .setNegativeButtonText("Abbrechen")
            .setAllowedAuthenticators(BIOMETRIC_STRONG)
            .build()
        biometricPrompt.authenticate(promptInfo)
    }
}