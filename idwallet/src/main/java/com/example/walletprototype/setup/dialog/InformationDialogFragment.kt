package com.example.walletprototype.setup.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.text.method.LinkMovementMethod
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.fragment.app.DialogFragment
import com.example.walletprototype.R
import com.example.walletprototype.databinding.FragmentInformationDialogBinding

class InformationDialogFragment(@StringRes val description: Int, @DrawableRes val image: Int) : DialogFragment() {

    private var _binding: FragmentInformationDialogBinding? = null
    private val binding get() = _binding!!

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            _binding = FragmentInformationDialogBinding.inflate(inflater, null, false)
            binding.imageViewSetupDialog.setImageResource(image)
            binding.textViewSetupDialog.setText(description)
            binding.textViewSetupDialog.movementMethod = LinkMovementMethod.getInstance()
            binding.textViewSetupDialog.setLinkTextColor(requireContext().getColor(R.color.md_theme_light_onBackground))
            binding.imageButtonClose.setOnClickListener {
                dismiss()
            }
            builder.setView(binding.root)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}