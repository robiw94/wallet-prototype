package com.example.walletprototype.setup.newpassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.walletprototype.R
import com.example.walletprototype.databinding.FragmentSetupNewPasswordBinding
import com.example.walletprototype.util.AuthenticationMethod

class SetupNewPasswordFragment : Fragment() {

    private var _binding: FragmentSetupNewPasswordBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSetupNewPasswordBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.setupCardPassword.setOnClickListener {
            val action =
                SetupNewPasswordFragmentDirections.actionSetupNewPasswordFragmentToPasswordSetupFragment(
                    AuthenticationMethod.PASSWORD
                )
            findNavController().navigate(action)
        }

        binding.setupCardPin.setOnClickListener {
            val action =
                SetupNewPasswordFragmentDirections.actionSetupNewPasswordFragmentToPinSetupFragment(
                    AuthenticationMethod.PIN
                )
            findNavController().navigate(action)
        }

        binding.setupCardFingerprintAndPassword.setOnClickListener {
            val action =
                SetupNewPasswordFragmentDirections.actionSetupNewPasswordFragmentToSetupFingerprintFragment(
                    AuthenticationMethod.FINGERPRINT_PASSWORD
                )
            findNavController().navigate(action)
        }

        binding.setupCardFingerprintAndPin.setOnClickListener {
            val action =
                SetupNewPasswordFragmentDirections.actionSetupNewPasswordFragmentToSetupFingerprintFragment(
                    AuthenticationMethod.FINGERPRINT_PIN
                )
            findNavController().navigate(action)
        }
    }
}