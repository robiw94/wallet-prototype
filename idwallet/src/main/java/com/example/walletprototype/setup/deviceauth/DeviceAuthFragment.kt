package com.example.walletprototype.setup.deviceauth

import android.app.Activity.RESULT_OK
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.provider.Settings
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL
import androidx.biometric.BiometricManager.BIOMETRIC_SUCCESS
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.walletprototype.R
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.databinding.FragmentDeviceAuthBinding
import com.example.walletprototype.databinding.FragmentSetupFingerprintBinding
import com.example.walletprototype.setup.newpassword.SetupFingerprintFragmentArgs
import com.example.walletprototype.setup.newpassword.SetupFingerprintFragmentDirections
import com.example.walletprototype.util.AuthenticationMethod
import com.example.walletprototype.util.PreferenceKeys
import kotlinx.coroutines.runBlocking
import java.util.concurrent.Executor

class DeviceAuthFragment : Fragment() {

    private var _binding: FragmentDeviceAuthBinding? = null
    private val binding get() = _binding!!

    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo
    private val authMethod: AuthenticationMethod = AuthenticationMethod.SYSTEM_AUTH
    private var isViewSetup = true


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDeviceAuthBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        if(!isViewSetup && Build.VERSION.SDK_INT >= 30) {
            handleBiometricAuth()
        }
        isViewSetup = false
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isViewSetup = true

        binding.buttonCancel.setOnClickListener {
            findNavController().popBackStack()
        }

        handleBiometricAuth()



    }

    private fun handleBiometricAuth() {
        if (Build.VERSION.SDK_INT >= 30) {
            if (BiometricManager.from(requireContext())
                    .canAuthenticate(BIOMETRIC_STRONG or DEVICE_CREDENTIAL) != BIOMETRIC_SUCCESS
            ) {
                binding.textViewFingerprintSetup.text =
                    getString(R.string.device_auth_not_enrolled)
                binding.imageViewFingerprint.setImageResource(R.drawable.baseline_remove_circle_128)
                binding.buttonContinue.text = getString(R.string.fingerprint_setup_now)
                binding.buttonContinue.visibility = View.VISIBLE
                binding.buttonContinue.isEnabled = true
                binding.buttonContinue.setOnClickListener {
                    val enrollIntent = Intent(Settings.ACTION_BIOMETRIC_ENROLL)
                    enrollIntent.putExtra(
                        Settings.EXTRA_BIOMETRIC_AUTHENTICATORS_ALLOWED,
                        BIOMETRIC_STRONG or DEVICE_CREDENTIAL
                    )
                    startActivity(enrollIntent)
                }
                return
            } else {
                binding.textViewFingerprintSetup.text =
                    getString(R.string.device_auth_description)
                binding.imageViewFingerprint.setImageResource(R.drawable.outline_fingerprint_128)
                binding.buttonContinue.visibility = View.GONE
            }

            executor = ContextCompat.getMainExecutor(requireContext())
            biometricPrompt = BiometricPrompt(this, executor,
                object : BiometricPrompt.AuthenticationCallback() {
                    override fun onAuthenticationError(
                        errorCode: Int,
                        errString: CharSequence
                    ) {
                        super.onAuthenticationError(errorCode, errString)
                        binding.textViewFingerprintSetup.text =
                            getString(R.string.setup_device_auth_failed)
                        val waitTime = 30L
                        binding.textViewRetryCountdown.visibility = View.VISIBLE
                        binding.textViewRetryCountdown.text = waitTime.toString()
                        object : CountDownTimer(waitTime * 1000, 1000) {
                            override fun onTick(millisUntilFinished: Long) {
                                binding.textViewRetryCountdown.text =
                                    (millisUntilFinished / 1000).toString()
                            }

                            override fun onFinish() {
                                binding.buttonContinue.isEnabled = true
                                binding.textViewRetryCountdown.visibility = View.GONE
                            }
                        }.start()
                        binding.imageViewFingerprint.visibility = View.GONE
                        binding.buttonContinue.setOnClickListener {
                            biometricPrompt.authenticate(promptInfo)
                        }
                        binding.buttonContinue.isEnabled = false
                        binding.buttonContinue.text = getString(R.string.retry)
                        binding.buttonContinue.visibility = View.VISIBLE
                    }

                    override fun onAuthenticationSucceeded(
                        result: BiometricPrompt.AuthenticationResult
                    ) {
                        super.onAuthenticationSucceeded(result)
                        binding.textViewFingerprintSetup.text =
                            getString(R.string.setup_device_auth_success)
                        binding.imageViewFingerprint.visibility = View.VISIBLE
                        binding.imageViewFingerprint.setImageResource(R.drawable.baseline_check_circle_128)
                        binding.buttonContinue.setOnClickListener {
                            runBlocking { setSetupCompleted() }
                            findNavController().navigate(R.id.action_deviceAuthFragment_to_setupDoneFragment)

                        }
                        binding.buttonContinue.text = getString(R.string.string_continue)
                        binding.buttonContinue.visibility = View.VISIBLE
                    }

                    override fun onAuthenticationFailed() {
                        super.onAuthenticationFailed()
                        binding.textViewFingerprintSetup.text =
                            getString(R.string.setup_fingerprint_failure)
                        binding.imageViewFingerprint.visibility = View.VISIBLE
                        binding.imageViewFingerprint.setImageResource(R.drawable.baseline_remove_circle_128)
                    }
                })

            promptInfo = BiometricPrompt.PromptInfo.Builder()
                .setTitle("Login mittels Gerätentsperrmethode Einrichten")
                .setSubtitle("Login per Gerätentsperrmethode")
                .setAllowedAuthenticators(BIOMETRIC_STRONG or DEVICE_CREDENTIAL)
                .build()
            biometricPrompt.authenticate(promptInfo)
        } else {
            val keyGuardManager =
                requireContext().getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager

            val intent = keyGuardManager.createConfirmDeviceCredentialIntent(
                "Login mittels Gerätentsperrmethode Einrichten",
                "Login per Gerätentsperrmethode"
            )
            startActivityForResult(intent, 42)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode  == 42){
            if(resultCode == RESULT_OK) {
                binding.textViewFingerprintSetup.text =
                    getString(R.string.setup_device_auth_success)
                binding.imageViewFingerprint.visibility = View.VISIBLE
                binding.imageViewFingerprint.setImageResource(R.drawable.baseline_check_circle_128)
                binding.buttonContinue.setOnClickListener {
                    runBlocking { setSetupCompleted() }
                    findNavController().navigate(R.id.action_deviceAuthFragment_to_setupDoneFragment)

                }
                binding.buttonContinue.text = getString(R.string.string_continue)
                binding.buttonContinue.visibility = View.VISIBLE
            } else {
                binding.textViewFingerprintSetup.text =
                    getString(R.string.setup_device_auth_failed)
                val waitTime = 30L
                binding.textViewRetryCountdown.visibility = View.VISIBLE
                binding.textViewRetryCountdown.text = waitTime.toString()
                object : CountDownTimer(waitTime * 1000, 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                        binding.textViewRetryCountdown.text =
                            (millisUntilFinished / 1000).toString()
                    }

                    override fun onFinish() {
                        binding.buttonContinue.isEnabled = true
                        binding.textViewRetryCountdown.visibility = View.GONE
                    }
                }.start()
                binding.imageViewFingerprint.visibility = View.GONE
                binding.buttonContinue.setOnClickListener {
                    biometricPrompt.authenticate(promptInfo)
                }
                binding.buttonContinue.isEnabled = false
                binding.buttonContinue.text = getString(R.string.retry)
                binding.buttonContinue.visibility = View.VISIBLE
            }
        }
    }

    private suspend fun setSetupCompleted() {
        WalletPrototypeApp.preferenceStore.storeValue(PreferenceKeys.SETUP_COMPLETED, true)
        WalletPrototypeApp.preferenceStore.storeValue(
            PreferenceKeys.AUTHENTICATION_METHOD,
            authMethod.ordinal
        )
    }
}