package com.example.walletprototype.setup

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import com.example.walletprototype.MainActivity
import com.example.walletprototype.WalletPrototypeApp
import com.example.walletprototype.databinding.FragmentSetupDoneBinding
import kotlinx.coroutines.runBlocking

class SetupDoneFragment : Fragment() {

    private var _binding: FragmentSetupDoneBinding? = null
    private val binding get() = _binding!!

    private lateinit var timer: CountDownTimer

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSetupDoneBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        runBlocking {
            WalletPrototypeApp.loginState.login()
        }

        timer = object : CountDownTimer(4000, 40) {
            override fun onTick(millisUntilFinished: Long) {
                binding.progressBarContinue.incrementProgressBy(1)
            }

            override fun onFinish() {
                val nextActivity = Intent(requireContext(), MainActivity::class.java)
                startActivity(nextActivity)
                requireActivity().finish()
            }
        }.start()

        binding.buttonContinue.setOnClickListener {
            timer.cancel()
            val nextActivity = Intent(requireContext(), MainActivity::class.java)
            startActivity(nextActivity)
            requireActivity().finish()
        }

        requireActivity().onBackPressedDispatcher.addCallback {
            timer.cancel()
            val nextActivity = Intent(requireContext(), MainActivity::class.java)
            startActivity(nextActivity)
            requireActivity().finish()
        }
    }
}